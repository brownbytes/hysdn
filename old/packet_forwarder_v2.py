### LLDPv5 in test####

#####
# author : durga
# module : packet_forwarder
# date :  19 nov
#####
# - Question : Have I done better?
# packetin module - checks for destination mac addr
# if dest mac  = ospf dest mac 01.00.5e.00.00.05 ,01.00.5e.00.00.06
#	- forward the packet to quaggaRS OVS with dpid :ba-95-5e-4d-ea-4b
#	- continue normal packet forwarding process 
#####

from pox.lib import *
from pox.core import core
from pox.lib.packet.lldp import * #import all classes from lldp module lldp(),chassis_id(),port_id() etc..
from pox.lib.packet.ethernet import ethernet,ETHER_BROADCAST,NDP_MULTICAST
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
from pox.lib.util import dpid_to_str,str_to_dpid
from pox.lib.addresses import EthAddr
import pathfinder
from lldpv3 import *
import pickle

log = core.getLogger()

#pickle SWITCHES.txt and HOSTS.txt'
_dummydict = {}
with open ('SWITCHES.txt','r+') as fd3:
        pickle.dump(_dummydict,fd3)
with open('HOSTS.txt','r+') as fd4:
        pickle.dump(_dummydict,fd4)

def launch():
    """ launch and register custome component to core """
    log.debug("in launch...")
    core.registerNew(MainMapMaker)
		
		
class MainMapMaker(object):
    """ handles the connections and ofp packets """

    def __init__(self):
        """ adds itself as listener to core.openflow and initialises switch_port dict"""
        core.openflow.addListeners(self)
        self.switch_ports = {} #temp stores switches and connected ports on the switch
        self.lldpobj = MapMaker()
        

    def _handle_ConnectionUp(self,event):
        """ handler for new connections. maintains switch_port dicts"""
        self.connection = event.connection
        self.ofp = event.ofp
        _ports = []
        log.debug("New Connection established to switch %s",dpid_to_str(event.dpid))
        log.debug("new event details %s", str(event))
        self.insertDefaultFlow(event.connection)

        for port in self.ofp.ports:
            _ports.append((port.port_no,port.hw_addr))
        self.switch_ports[dpid_to_str(self.connection.dpid)]= _ports
        log.debug("%s",str(self.switch_ports))
        self.sendLLDP(event)

    def insertDefaultFlow(self,connection):
        """ method to insert default flow of send to controller , called in connectionup handler"""
        msg = of.ofp_flow_mod()
        msg.actions.append(of.ofp_action_output(port=of.OFPP_CONTROLLER))
        connection.send(msg)

    def updateMap(self):
        """ method to  perform updates on map """
        pass

    def _handle_PacketIn(self,event):
        """ handler for packetIn events """
        self.processPacketIn(event)
		
    def processPacketIn(self,event):
        """ method to process incoming packets """
        parsedpkt = event.parsed
        inport = event.port
        srcdpid = event.dpid
        #log.debug("%i --> %s",event.dpid,parsedpkt)
        if parsedpkt.type == parsedpkt.LLDP_TYPE: #process LLDP packet
            print "processing LLDP Packetin"
            self.lldpobj.packetin_LLDP(event)
            path = pathfinder.find_path(1)
            print path

	elif parsedpkt.dst == EthAddr('01:00:5e:00:00:05') or parsedpkt.dst == EthAddr('01:00:5e:00:00:06'):
            print "processing ospf packet"
	    #print str(parsedpkt)
            self.sendToRSE(event)
		        

    def sendLLDP(self,event):
        """ instruct switches to send LLDP packet Out messages ,iterate on switch_port"""
        for outport in self.switch_ports[dpid_to_str(event.dpid)]: # send packeout of lldps on all ports exce$
            #print outport (2.EthAddr('xx:xx:xx:xx:xx:xx')
            msg = of.ofp_packet_out(in_port = of.OFPP_NONE)
            if outport[0] == 65534:
                pass
            else:
                lldpdu = self.lldpobj.buildEther(event.dpid,outport)# (ddpid,2,EthAddr('xx:xx:xx:xx:xx:xx')) 
                msg.actions.append(of.ofp_action_output(port=outport[0]))
                msg.data = lldpdu.pack()
                #log.debug("OF %s",str(msg))
                self.connection.send(msg)
    
    def sendToRSE(self,event):
        """ forward the routing update to routing engine via internal 
            ovs quaggaRS """
        msg = of.ofp_packet_out(in_port = of.OFPP_NONE)
        if event.port == 1: #for lxc1 and lxc2 testing only
            outport = 2
        elif event.port == 2:
            outport = 1
        legacyL3payload = event.parsed
        msg.actions.append(of.ofp_action_output(port=outport))
        
        msg.data = legacyL3payload.pack()
        core.openflow.sendToDPID(str_to_dpid('ba-95-5e-4d-ea-4b'),msg)

    def getSwitchPort(self):
        """ retunrs switch _port when called in methods outside this class """
        return self.switch_port



