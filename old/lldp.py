#####
# author : durga
# module : LLDP
# date : 19 june
#####
# - frame LLDP packets
# - send PacketOut messages carrying LLDP packets
# - Question : Have I done better?
# phase1 - inserts initial flowod
# phase2 - frame and send LLDP packets using PacketOut messages
# phase3 - read the packetin messages containing LLDP packet and make a graph file

#####

from pox.lib import *
from pox.core import core
from pox.lib.packet.lldp import * #import all classes from lldp module lldp(),chassis_id(),port_id() e$
from pox.lib.packet.ethernet import ethernet,ETHER_BROADCAST,NDP_MULTICAST
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
from pox.lib.util import dpid_to_str,str_to_dpid
from pox.lib.addresses import EthAddr

log = core.getLogger()



class MapMaker(object):
    """ build LLDP packets , creates a graph input : s,d pair , output : shortest distance """

    log.debug('connected to MapMaker module')

    def __init__(self,dpid,port):

        self.dpid = dpid
        self.port = port
        self._temp_map = {} # temprarily stores mappings
		
    def buildLLDPDU(self,dpid,port):
        """ build and send LLDP"""
        lldpdu = lldp() # creates  lldp obj
        #tlvs=[] is defined in class lldp in lib.packet.lldp

        """ chassis TLV """
        chassisTLV = chassis_id() #create a chassis obj
        chassisTLV.subtype = chassisTLV.SUB_LOCAL# why sub_local
        chassisTLV.id = bytes(hex(long(dpid))[2:-1])
        lldpdu.add_tlv(chassisTLV) #add to the list of TLVs 

        """ Port TLV """
        portTLV = port_id()
        portTLV.subtype = portTLV.SUB_PORT
        portTLV.id = str(port) # test portTLV.id to all values in self.switch_port[dpid]
        lldpdu.add_tlv(portTLV)

        """ TTLTLV """
        ttlTLV = ttl()
        ttlTLV.ttl = 120 #120secs
        lldpdu.tlvs.append(ttlTLV)

        """ ENDTLV """
        endtlv = end_tlv()
        lldpdu.tlvs.append(endtlv)
        #log.debug(" %s", str(lldpdu))
        return lldpdu
		
		
	def buildEther(self,dpid,port): #dpid,((2,EthAddr('xx:xx:xx:xx:xx:xx'))
        """ encapsulating in ethernet """
        etherLLDPDU = self.buildLLDPDU(dpid,port[0])
        ether = ethernet()
        ether.type = ethernet.LLDP_TYPE
        ether.src = port[1] #ethernet src should be the phy addr of port from where the packet is sent$
        ether.dst = EthAddr('01:80:c2:00:00:0e') #LLDP_multicast
        ether.set_payload(etherLLDPDU)
        #log.debug("Ethernet %s",str(ether))
        return ether

    def packetin_LLDP(self,event):
        """ reading the parsed packet and updating the src and destination """
        parsedpkt = event.parsed
        chassis2 = hex(long(event.dpid))[2:-1]
        port2 = event.port
        LLDPpkt = parsedpkt.payload
        log.debug("%s",LLDPpkt.tlvs)
        chassis1 = LLDPpkt.tlvs[0].id
        port1 = LLDPpkt.tlvs[1].id
        self._temp_map[(chassis1,port1)] = [(chassis2,port2)]
        print self._temp_map

def launch():
    """ launch and register custome component to core """
    log.debug("in launch...")
    core.registerNew(MainMapMaker)
	
class MainMapMaker(object):
    """ handles the connections and ofp packets """

    def __init__(self):
        """ adds itself as listener to core.openflow and initialises switch_port dict"""
        core.openflow.addListeners(self)
        self.switch_ports = {}
        self.mapobj = MapMaker(dpid=0,port=0)

    def _handle_ConnectionUp(self,event):
        """ handler for new connections. maintains switch_port dicts"""
        self.connection = event.connection
        self.ofp = event.ofp
        _ports = []
        log.debug("New Connection established to switch %s",dpid_to_str(event.dpid))
        log.debug("new event details %s", str(event))
        self.insertDefaultFlow(event.connection)

        for port in self.ofp.ports:
            _ports.append((port.port_no,port.hw_addr))
        self.switch_ports[dpid_to_str(self.connection.dpid)]= _ports
        log.debug("%s",str(self.switch_ports))
        self.sendLLDP(event)

    def insertDefaultFlow(self,connection):
        """ method to insert default flow of send to controller , called in connectionup handler"""
        msg = of.ofp_flow_mod()
        msg.actions.append(of.ofp_action_output(port=of.OFPP_CONTROLLER))
        connection.send(msg)
		
    def updateMap(self):
        """ method to  perform updates on map """
        pass

    def _handle_PacketIn(self,event):
        """ handler for packetIn events """
        self.processPacketIn(event)

    def sendFlow(self):
        """ method to send flow table updates """
        pass

    def processPacketIn(self,event):
        """ method to process incoming packets """
        parsedpkt = event.parsed
        inport = event.port
        srcdpid = event.dpid
        #log.debug("%i --> %s",event.dpid,parsedpkt)
        if parsedpkt.type == parsedpkt.LLDP_TYPE: #process LLDP packet
            print "processing LLDP Packetin"
            self.mapobj.packetin_LLDP(event)

    def sendLLDP(self,event):
        """ instruct switches to send LLDP packet Out messages ,iterate on switch_port"""
        for outport in self.switch_ports[dpid_to_str(event.dpid)]: # send packeout of lldps on all por$
            #print outportlist (2.EthAddr('xx:xx:xx:xx:xx:xx')
            msg = of.ofp_packet_out(in_port = of.OFPP_NONE)
            if outport[0] == 65534:
                pass
            else:
                lldpdu = self.mapobj.buildEther(event.dpid,outport)# (ddpid,2,EthAddr('xx:xx:xx:xx:xx:$
                msg.actions.append(of.ofp_action_output(port=outport[0]))
                msg.data = lldpdu.pack()
                #log.debug("OF %s",str(msg))
                self.connection.send(msg)

    def getSwitchPort(self):
        """ retunrs switch _port when called in methods outside this class """
        return self.switch_port

