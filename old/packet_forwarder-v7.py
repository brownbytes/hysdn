#####
# author : durga
# module : main process flow module 
# date :  18 dec
#####
"""
this is the primary decision making process
the process validates an incoming packet as either a control paceket or a data packet
for all control packets, based on whether they are a routing update or LLDP packet
the respective tables are updated.
for routing updates, the packet is forwarded to the RS to update the routing tables
incase of LLDP packets, lldp packets corresponding actions are taken

for all data packets, based on the destination address , either local address module 
to track addresses is refered to or the routing table is refered for next egress router
also, another table is maintained to map the egree of nodes to the legacy routers to decide a optimal egress node

"""
#####

from pox.lib import *
from pox.core import core
from pox.lib.packet.lldp import * #import all classes from lldp module lldp(),chassis_id(),port_id() etc..
from pox.lib.packet.ethernet import ethernet,ETHER_BROADCAST,NDP_MULTICAST
from pox.lib.packet.ipv4 import ipv4
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
from pox.lib.util import dpid_to_str,str_to_dpid
from pox.lib.addresses import EthAddr,IPAddr,IP_ANY,IP_BROADCAST
import pathfinder
import query_route
from lldpv3 import *

import pickle
import sys
import linecache

log = core.getLogger()


def tracer(frame,event,arg):
    """ setting a trace function to check the event flows"""
    filename = frame.f_code.co_filename
    linenumber = frame.f_lineno
    funcname = frame.f_code.co_name
    line = linecache.getline(filename,linenumber)
    info = "%s: %s in %s" %(linenumber,line,funcname)
    print info


#pickle SWITCHES.txt,HOSTS.txt and EDGES.txt'
_dummydict = {}
with open('SWITCHES.txt','r+') as fd3:
        pickle.dump(_dummydict,fd3)
with open('HOSTS.txt','r+') as fd4:
        pickle.dump(_dummydict,fd4)
with open('EDGES.txt','r+') as fd5:
        pickle.dump(_dummydict,fd5)

ethertypes = ['IP_TYPE','ARP_TYPE','RARP_TYPE','VLAN_TYPE','LLDP_TYPE','PAE_TYPE','MPLS_TYPE','MPLS_MC_TYPE','IPV6_TYPE','PPP_TYPE','LWARPP_TYPE','GSMP_TYPE','IPX_TYPE','IPX_TYPE','WOL_TYPE','TRILL_TYPE','JUMBO_TYPE','SCSI_TYPE','ATA_TYPE','QINQ_TYPE']
def launch():
    """ launch and register custome component to core """
    log.debug("in launch...")
    core.registerNew(MainMapMaker)
				
class MainMapMaker(object):
    """ handles the connections and ofp packets """

    def __init__(self):
        """ adds itself as listener to core.openflow and initialises switch_port dict"""
        core.openflow.addListeners(self)
        self.switch_ports = {} #temp stores switches and connected ports on the switch
        self.edge_switch = {} #temp dict to store edge OF switches
        self.lldpobj = MapMaker()
        
    def _handle_ConnectionUp(self,event):
        """ handler for new connections. maintains switch_port dicts"""
        self.connection = event.connection
        self.ofp = event.ofp
        _ports = []
        log.debug("New Connection established to switch %s",dpid_to_str(event.dpid))
        log.debug("new event details %s", str(event))
        self.insertDefaultFlow(event.connection)

        for port in self.ofp.ports:
            _ports.append((port.port_no,port.hw_addr))
        self.switch_ports[dpid_to_str(self.connection.dpid)]= _ports
        #log.debug("%s",str(self.switch_ports))
        self.sendLLDP(event)

    def insertDefaultFlow(self,connection):
        """ method to insert default flow of send to controller , called in connectionup handler"""
        msg = of.ofp_flow_mod()
        #msg.priority = -1
        msg.actions.append(of.ofp_action_output(port=of.OFPP_CONTROLLER))
        connection.send(msg)

    def updateMap(self):
        """ method to  perform updates on map """
        pass

    def _handle_PacketIn(self,event):
        """ handler for packetIn events """
        #sys.settrace(tracer)
        self.processPacketIn(event)
        #sys.settrace(None)
		
    def processPacketIn(self,event):
        """ method to process incoming packets """
        parsedpkt = event.parsed
        inport = event.port
        srcdpid = event.dpid
        pktpayload = parsedpkt.payload
        #log.debug("%i --> %s",event.dpid,parsedpkt)
        if parsedpkt.type == parsedpkt.LLDP_TYPE: #1. process LLDP packet
            #print "processing LLDP Packetin"
            self.lldpobj.packetin_LLDP(event)
            
	elif parsedpkt.dst == EthAddr('01:00:5e:00:00:05') or parsedpkt.dst == EthAddr('01:00:5e:00:00:06'): #2
            #print "processing legacy OSPF routing update packet"
            #self.sendrouteupdate(event)
            #update the switch_router dict {sw1:[(p1,r1ip),(p2,r2ip)..],sw2:[...]}
            ospfpkt = pktpayload#IP pkt within the parsedpkt
     
            if event.dpid in self.edge_switch:
                for each_sw in self.edge_switch:
                    if each_sw == event.dpid:
                        if (event.port,ospfpkt.srcip) not in self.edge_switch[each_sw]:
                                self.edge_switch[event.dpid].append((event.port,ospfpkt.srcip))
            else:
                self.edge_switch[event.dpid]=[(event.port,ospfpkt.srcip)]
            #print self.edge_switch
            self.sendrouteupdate(event)

        elif parsedpkt.type == parsedpkt.ARP_TYPE: # need to extend to other ethernet types of packet
            #print "processing ARP data pkt"
            #cntrl checks for edgerouter file, and forwards the arp packet to relevant switch
            arppkt = parsedpkt.payload
            edge = self.findEgress(arppkt.protodst)
            if edge != (0,0):
                self.controller_unicast(event,edge[1],edge[0]) #if edge
            else: #if edge is 0 ie not an edge router, handle the packet differently
                pass #look in hosts file to check destination mac addr #findHost

        elif parsedpkt.type == parsedpkt.IP_TYPE:
            #sys.settrace(tracer)
            print "processing IP data pkt"
            ippkt = parsedpkt.payload
            #print "processing %s IP data pkt to %s" %(ippkt.protocol,ippkt.dstip)
            # for ospf, need to move ths to better module
            edge = self.findEgress(ippkt.dstip)
            #print edge
            if edge != (0,0): #if a edge device
                #print "edge device"
                self.controller_unicast(event,edge[1],edge[0])
            else : # if internal network, then find next hop
                next_hop_node = query_route.next_hop_of(str(IPAddr(ippkt.dstip)))
                print next_hop_node # its the edge router 
                if next_hop_node != 0: # if next hop of the packet exists
            #check the egress node based on the nexthop IP
            # frame a new message
            #send the packet to the egress node to be forwarded onto the interface connecting to the router
                    edge = self.findEgress(IPAddr(next_hop_node))
                    print edge
                    path = pathfinder.find_path(event.dpid,edge[0])#find the path to egreeswitch within OF domain
                    print path
                    self.installFlows(edge,parsedpkt,event)  
                    for dstsw in path:
                    	self.installFlows(path[dstsw],parsedpkt,event)
                    #self.installFlows(edge,ippkt,event)
            #sys.settrace(None)

    def sendLLDP(self,event):
        """ instruct switches to send LLDP packet Out messages ,iterate on switch_port"""
        for outport in self.switch_ports[dpid_to_str(event.dpid)]: # send packeout of lldps on all ports exce$
            #print outport (2.EthAddr('xx:xx:xx:xx:xx:xx')
            msg = of.ofp_packet_out(in_port = of.OFPP_NONE)
            if outport[0] == 65534:
                pass
            else:
                lldpdu = self.lldpobj.buildEther(event.dpid,outport)# (ddpid,2,EthAddr('xx:xx:xx:xx:xx:xx')) 
                msg.actions.append(of.ofp_action_output(port=outport[0]))
                msg.data = lldpdu.pack()
                #log.debug("OF %s",str(msg))
                self.connection.send(msg)
    
    def sendrouteupdate(self,event):
        """ forward the routing update to routing engine via internal 
            ovs quaggaRS """
        routeupdatepkt = event.parsed
        msg = of.ofp_packet_out(in_port = of.OFPP_NONE)
        msg.data = routeupdatepkt.pack()
        for each_switch in self.edge_switch:
            for each_pr in self.edge_switch[each_switch]:
                if each_pr[0] != event.port:
                    msg.actions.append(of.ofp_action_output(port=each_pr[0])) #need to update it as per egress ports
                    core.openflow.sendToDPID(each_switch,msg)
    
    def forward_pkt(self,event,outport):
        """instructs the OF switch to forward the packet onto an egress node"""
        msg = of.ofp_packet_out(in_port = of.OFPP_NONE)
        msg.actions.append(of.ofp_action_output(port=outport))
        msg.data = event.data        
        self.connection.send(msg)#need to send to the swicth
        
    def controller_unicast(self,event,outport,swdpid):
        """ this method is used when controller has to direct unicast messages from and to RS
        """
        ippkt = event.parsed
        msg = of.ofp_packet_out(in_port = of.OFPP_NONE)
        msg.actions.append(of.ofp_action_output(port=outport))
        msg.data = ippkt.pack()
        core.openflow.sendToDPID(swdpid,msg)

    def findEgress(self,ipaddr):
        """checks if the IP address belongs to any of the border routers and returns the sw and port which connect to it"""
        is_edge = 0 #flag to indicate whether the ip addr is found in the
 
        for each_sw in self.edge_switch:
            for each_pr in self.edge_switch[each_sw]:
                if each_pr[1] == ipaddr:
                    outport = each_pr[0]
                    egrssw = each_sw
                    is_edge = 1
                    break
                else:
                    continue        
        if is_edge == 1: # the node is edge routee
            return egrssw,outport
        elif is_edge == 0:# implies internal network, find next hop 
            return (0,0)          
    
    def findHost(self,ipaddr):
        """ to find hosts for arp purposes"""
        pass


    def installFlows(self,dstswid,parsedpkt,event):
        """install flows proactively on all the switches in the best path"""
        #pktpayload = ippkt.payload
        print "2"
        msg = of.ofp_flow_mod()
        msg.match = of.ofp_match.from_packet(parsedpkt,event.port)
        #msg.match.dl_type = 0x800
        #msg.match.nw_dst = IPAddr(ippkt.dstip)
        swdpid = dstswid[0] # source switch dpid
        outport = dstswid[1] #source switch outport
        #insert flowmod of a specific destination IP, outgoing on this port, on switchdpid
        msg.actions.append(of.ofp_action_output(port = outport))
        msg.data = event.ofp
        #print "swdpid %s,outport%s" %(str(swdpid),str(outport))
        core.openflow.sendToDPID(swdpid,msg)

    def getSwitchPort(self):
        """ returns switch _port when called in methods outside this class """
        return self.switch_port



