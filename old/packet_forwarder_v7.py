#####
# author : durga
# module : main process flow module 
# date :  23 apr # last working version
#####
"""
this is the primary decision making process
the process validates an incoming packet as either a control packet or a data packet
for all control packets, based on whether they are a routing update or LLDP packet
the respective tables are updated.
for routing updates, the packet is forwarded to the RS to update the routing tables
incase of LLDP packets, lldp packets corresponding actions are taken

for all data packets, based on the destination address , either local address module 
to track addresses is refered to or the routing table is refered for next egress router
also, another table is maintained to map the egress of nodes to the legacy routers to decide a optimal egress node

"""

from pox.lib import *
from pox.core import core
from pox.lib.packet.lldp import * # import all classes from lldp module lldp(),chassis_id(),port_id() etc..
from pox.lib.packet.ethernet import ethernet, ETHER_BROADCAST, NDP_MULTICAST
from pox.lib.packet.ipv4 import ipv4
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
from pox.lib.util import dpid_to_str, str_to_dpid
from pox.lib.addresses import EthAddr, IPAddr, IP_ANY, IP_BROADCAST
import pathfinder
import query_route
from lldpv3 import *

import pickle
import sys
import linecache

log = core.getLogger()


def tracer(frame, event, arg):
    """ setting a trace function to check the event flows"""
    filename = frame.f_code.co_filename
    linenumber = frame.f_lineno
    funcname = frame.f_code.co_name
    line = linecache.getline(filename, linenumber)
    info = "%s: %s in %s" %(linenumber, line, funcname)
    print info

ethertypes = ['IP_TYPE', 'ARP_TYPE', 'RARP_TYPE', 'VLAN_TYPE', 'LLDP_TYPE', 'PAE_TYPE', 'MPLS_TYPE', 'MPLS_MC_TYPE', 'IPV6_TYPE', 'PPP_TYPE', 'LWARPP_TYPE', 'GSMP_TYPE', 'IPX_TYPE', 'IPX_TYPE', 'WOL_TYPE', 'TRILL_TYPE', 'JUMBO_TYPE', 'SCSI_TYPE', 'ATA_TYPE', 'QINQ_TYPE']

def launch():
    """ launch and register custom component to core """
    log.debug("in launch...")
    core.registerNew(MainMapMaker)
				
class MainMapMaker(object):
    """handles the connections and ofp packets"""

    def __init__(self):
        """ adds itself as listener to core.openflow and initialises switch_port dict"""
        core.openflow.addListeners(self)
        self.switch_ports = {}  # temp stores switches and connected ports on the switch
        self.edge_switch = {}  # temp dict to store edge OF switches
        self.lldpobj = MapMaker()
        self.switch_hosts = {}  # stores OFswitches and associated hosts IPs
        self.edge_arp = {}  # stores edge IP MAC mapping details
       
    def _handle_ConnectionUp(self, event):
        """ handler for new connections. maintains switch_port dicts"""

        self.connection = event.connection
        self.ofp = event.ofp
        _ports = []
        log.debug("New Connection established to switch %s", dpid_to_str(event.dpid))
        log.debug("new event details %s", str(event))
        self.insertDefaultFlow(event.connection)

        for port in self.ofp.ports:
            _ports.append((port.port_no, port.hw_addr))
        self.switch_ports[event.dpid]= _ports
        log.debug("%s", str(self.switch_ports))
        self.sendLLDP(event)

    def insertDefaultFlow(self, connection):
        """ method to insert default flow of send to controller , called in connectionup handler"""
        msg = of.ofp_flow_mod()
        # msg.priority = -1
        msg.actions.append(of.ofp_action_output(port=of.OFPP_CONTROLLER))
        connection.send(msg)

    def updateMap(self):
        """ method to  perform updates on map """
        pass

    def _handle_PacketIn(self, event):
        """ handler for packetIn events """
        # sys.settrace(tracer)
        self.processPacketIn(event)
        # sys.settrace(None)
		
    def processPacketIn(self, event):
        """ method to process incoming packets """
        parsedpkt = event.parsed
        inport = event.port
        srcdpid = event.dpid
        pktpayload = parsedpkt.payload
        target_sw = {}
        isedge = 0

        log.debug("%i --> %s", event.dpid, parsedpkt)

        # handling LLDP packet
        if parsedpkt.type == parsedpkt.LLDP_TYPE:
            #print "processing LLDP Packetin"
            self.lldpobj.packetin_LLDP(event)

        # handling OSPF multicast packets

        elif parsedpkt.dst == EthAddr('01:00:5e:00:00:05') or parsedpkt.dst == EthAddr('01:00:5e:00:00:06'): #2
            #print "1.processing legacy OSPF routing update packet"
            self.buildEdgeTable(event)  # builds edge table

            # update the switch_router dict {sw1:[(p1,r1ip),(p2,r2ip)..],sw2:[...]}
            # multicast the packet
            target_sw = self.multicastPacket(parsedpkt)
            self.controllerQueue(event, target_sw)  # controller queues the packet

        # handling ARP packets
        elif parsedpkt.type == parsedpkt.ARP_TYPE:
            arppkt = parsedpkt.payload
            edge_sw = self.findEgress(arppkt.protodst)
            #print "processing ARP ", edge_sw, arppkt.protodst
            if edge_sw != (0, 0):  # if edge switch, the controller enqueues the packet
                target_sw[edge_sw[0]] = [edge_sw[1]]
                self.controllerQueue(event, target_sw)
                self.buildEdgeARP(arppkt)
                print self.edge_arp
            elif edge_sw == (0, 0):  # if not an edge device
                self.buildHostTable(event, parsedpkt)  # build host table
                try:   # if dstip in hosttable
                    sw = self.switch_hosts[arppkt.protodst]  # tht is our target sw
                    target_sw[sw[0]] = [sw[1]]
                    self.controllerQueue(event, target_sw)
                except KeyError:  # if not found in host table multicast it
                    target_sw = self.multicastPacket(parsedpkt)
                    self.controllerQueue(event, target_sw)

        elif parsedpkt.type == parsedpkt.IP_TYPE:
            print "processing IP data pkt"
            ippkt = parsedpkt.payload
            #print "processing %s IP data pkt to %s" %(ippkt.protocol, ippkt.dstip)
            target_sw = self.unicastPacket(parsedpkt)

            #print "unicast TS", target_sw
            if target_sw != {}:  # if unicast finds a target sw
                for sw in self.edge_switch:  # if edge router
                    for p in self.edge_switch[sw]:
                        if p[1] == ippkt.dstip:
                            #print "***in edge loop***"
                            isedge = 1
                            self.controllerQueue(event, target_sw)#can be replaced by flowinstall
                            break
                if isedge == 0:
                    self.controllerFlowInstall(event,target_sw)
            elif target_sw == {}:  # if unicasting did not find target_Sw multicast the packet
                target_sw = self.multicastPacket(parsedpkt)
                self.controllerQueue(event, target_sw)
                
    def sendLLDP(self, event):
        """ instruct switches to send LLDP packet Out messages ,iterate on switch_port"""
        #print "Sending LLDP"
        for outport in self.switch_ports[event.dpid]:  # send packetout of lldps on all ports exce$
            # print outport (2.EthAddr('xx:xx:xx:xx:xx:xx')
            msg = of.ofp_packet_out(in_port=of.OFPP_NONE)
            if outport[0] == 65534:
                pass
            else:
                lldpdu = self.lldpobj.buildEther(event.dpid, outport)  # (ddpid,2,EthAddr('xx:xx:xx:xx:xx:xx'))
                msg.actions.append(of.ofp_action_output(port=outport[0]))
                msg.data = lldpdu.pack()
                log.debug("OF %s",str(msg))
                self.connection.send(msg)
    
    def forward_pkt(self, event, outport):
        """instructs the OF switch to forward the packet onto an egress node"""
        msg = of.ofp_packet_out(in_port=of.OFPP_NONE)
        msg.actions.append(of.ofp_action_output(port=outport))
        msg.data = event.data        
        self.connection.send(msg)  # need to send to the swicth

    def findEgress(self, ipaddr):
        """checks if the IP address belongs to any of the border routers and returns the sw and port which connect to it"""
        is_edge = 0  # flag to indicate whether the ip addr is found in the
        #print "finding egress to %s" %(ipaddr)
        for each_sw in self.edge_switch:
            for each_pr in self.edge_switch[each_sw]:
                if each_pr[1] == ipaddr:
                    outport = each_pr[0]
                    egrssw = each_sw
                    is_edge = 1
                    break
                else:
                    continue        
        if is_edge == 1:  # the node is edge router
            return egrssw, outport
        
        elif is_edge == 0:  # implies internal network, check if next hop exists using routeserver
            return (0,0)
    
    def buildHostTable(self, event, parsedpkt):
        """ builds host table"""
        
        # parsedpkt = event.parsed
        # extract arp packet
        arppkt = parsedpkt.payload
        srcip = arppkt.protosrc
        # do source based learning for ARP

        try:
            if self.switch_hosts[srcip] == (event.dpid, event.port):  # srcip exists for this switch
                pass  # do nothing
            else:
                self.switch_hosts[srcip] = (event.dpid, event.port)  # update latest information
        except KeyError:
            self.switch_hosts[srcip] = (event.dpid, event.port)

        #print "building host table",self.switch_hosts
        
    def installFlows(self, dstswid, parsedpkt,event):
        """install flows proactively on all the switches in the best path"""
        pktpayload = parsedpkt.payload

        msg = of.ofp_flow_mod()
        msg.match = of.ofp_match.from_packet(parsedpkt,event.port)
        msg.idle_timeout = 10
        # msg.match.dl_type = 0x800
        # msg.match.nw_dst = IPAddr(ippkt.dstip)
        swdpid = dstswid[0]  # source switch dpid
        outport = dstswid[1]  # source switch outport
        #print "installing flows on"  +str(swdpid) + "for port" + str(outport)

        # insert flowmod of a specific destination IP, outgoing on this port, on switchdpid

        msg.actions.append(of.ofp_action_output(port=outport))
        msg.data = event.ofp
        # print "swdpid %s,outport%s" %(str(swdpid),str(outport))
        core.openflow.sendToDPID(swdpid,msg)

    def installFlowsRemote(self,dstswid,parsedpkt,event):
        """based on the dstip update the destination mac addr feild of the packet"""
        pktpayload = parsedpkt.payload
        next_hop = query_route.next_hop_of(str(IPAddr(pktpayload.dstip)))
        tmac = self.edge_arp[IPAddr(next_hop)]
        print tmac,type(tmac)
        swdpid = dstswid[0]  # source switch dpid
        outport = dstswid[1]

        msg = of.ofp_flow_mod()
        msg.match = of.ofp_match.from_packet(parsedpkt,event.port)
        msg.actions.append(of.ofp_action_dl_addr.set_dst(tmac))
        msg.actions.append(of.ofp_action_output(port=outport))
        msg.data = event.ofp
        # print "swdpid %s,outport%s" %(str(swdpid),str(outport))
        core.openflow.sendToDPID(swdpid,msg)


    def getSwitchPort(self):
        """ returns switch _port when called in methods outside this class """
        return self.switch_port

    def multicastPacket(self, parsedpkt):
        """returns the traget_sw on which controller should perform some activity"""
        target_sw = {}
        dontMC = []
        #print "3. multicasting the packet to other switches"
        with open('SWITCHES.txt','r+') as fd8:
            temp_switch = pickle.load(fd8)
            pickle.dump(temp_switch,fd8)

        # determing all edge switches and ports connected to routers as target sws
        if parsedpkt.dst == EthAddr('01:00:5e:00:00:05') or parsedpkt.dst == EthAddr('01:00:5e:00:00:06'):
            for each_sw in self.edge_switch:
                for each_router in self.edge_switch[each_sw]:
                    try :
                        pr = target_sw[each_sw]
                        if each_router[0] in pr:
                            pass
                        else:
                            target_sw[each_sw].append(each_router[0])
                    except KeyError:
                        target_sw[each_sw]=[each_router[0]]

        # if not edge then look broadcast on all host ports of switch
        elif parsedpkt.dst == EthAddr('ff:ff:ff:ff:ff:ff'):
            for each_sw in temp_switch:
                for ports in temp_switch[each_sw]:
                    dontMC.append((each_sw,ports[0]))  # dont multicast on these ports

            for each_sw in self.switch_ports:
                for each_pr in self.switch_ports[each_sw]:
                    #print "each_pr", each_pr
                    if (each_sw, each_pr[0]) in dontMC :
                        pass
                    else:
                        try:
                            pr = target_sw[each_sw]
                            if each_pr in pr:
                                pass
                            else:
                                target_sw[each_sw].append(each_pr[0])
                        except KeyError:
                            target_sw[each_sw] = [each_pr[0]]
            #print "in multicasting", target_sw
        return target_sw


    def unicastPacket(self, parsedpkt):
        """ returns target sw for unicasting """
        pktpayload = parsedpkt.payload
        target_sw = {}
        #  print "unicast",self.switch_hosts, type(pktpayload.dstip)

        # check if edge device to unicast
        for each_sw in self.edge_switch:
            for each_r in self.edge_switch[each_sw]:
                if each_r[1] == pktpayload.dstip:
                    target_sw[each_sw] = [each_r[0]]
                    #print "unicast TS1",target_sw
                    return target_sw

        # if not an edge, check host table
        if pktpayload.dstip in self.switch_hosts:
            target_sw[self.switch_hosts[pktpayload.dstip][0]] = [self.switch_hosts[pktpayload.dstip][1]]
            #print "unicast TS2", target_sw
            return target_sw

        # if part of remote legacy network , compute edge sw as target sw
        else:
            next_hop_node = query_route.next_hop_of(str(IPAddr(pktpayload.dstip)))
            #print next_hop_node
            if next_hop_node != 0:
                edge_sw = self.findEgress(IPAddr(next_hop_node))
                #print "edge_sw"+str(edge_sw)
                if edge_sw != (0,0):
                    target_sw[edge_sw[0]]= [edge_sw[1]]
                    #print "unicast TS3", target_sw
                    return target_sw
        return target_sw

    def controllerQueue(self, event, target_sw):
        """controller directly queues the packet to target_sw"""
        #print "controller queue"
        for sw in target_sw:  # send on all ports except the incoming port
            for pr in target_sw[sw]:
                msg = of.ofp_packet_out(in_port = of.OFPP_NONE)
                msg.data = event.parsed.pack()
                if (pr, sw) != (event.port, event.dpid):
                    msg.actions.append(of.ofp_action_output(port=pr))
                    core.openflow.sendToDPID(sw, msg)

    def controllerFlowInstall(self, event, target_sw):
        """ controller installs flows on all the switches leading to target_sw"""
        #print "controller Flow Install"
        print "--------"
        print target_sw ,event.dpid
        parsedpkt = event.parsed
        dstipaddr = parsedpkt.payload.dstip
        for sw1 in target_sw: # for each target switch
            path = pathfinder.find_path(event.dpid, sw1)
            self.installFlows((sw1, target_sw[sw1][0]), parsedpkt, event)
            for sw2 in path: # for each intermediate node in the path to target switch
                self.installFlows((path[sw2][0], path[sw2][1]), parsedpkt, event)
                if sw2 in self.edge_switch: # if the switch in the path is an edge switch
                    print "^^^^",sw2
                    if not(self.isedge(dstipaddr)):
                        print "installing for remote"
                        self.installFlowsRemote((path[sw2][0], path[sw2][1]), parsedpkt, event) #change the mac addr and send the packet

    def isedge(self,ipaddr):
        """determines if an address is edge router interface or not"""
        for each_sw in self.edge_switch:
            for each_r in self.edge_switch[each_sw]:
                if each_r[1] == ipaddr:
                    return True

    def buildEdgeTable(self, event):
        """build edge table {dpid:[(swport,packetip)]}"""
        parsedpkt = event.parsed.payload

        #print "2.building edge table",self.edge_switch
        try :
            if (event.port, parsedpkt.srcip) in self.edge_switch[event.dpid]:
                pass
            else:
                self.edge_switch[event.dpid].append((event.port, parsedpkt.srcip))
        except KeyError:
            self.edge_switch[event.dpid] = [(event.port, parsedpkt.srcip)]

    def buildEdgeARP(self,arppkt):
        """build edge arp tables {ip:macaddr} using src based learning for arp packets"""

        #print "@@@@@"+str(arppkt.protosrc)+str(arppkt.hwsrc)
        self.edge_arp[IPAddr(arppkt.protosrc)] = EthAddr(arppkt.hwsrc)
        # try:
        #     if arppkt.protosrc in self.edge_arp:
        #         print "in arp1"
        #         if self.edge_arp[IPAddr(arppkt.protosrc)] == EthrAddr(arppkt.hwsrc):
        #             print "in arp2"
        #             pass
        #         else:
        #             self.edge_arp[IPAddr(arppkt.protosrc)] = EthrAddr(arppkt.hwsrc)
        #             print "in arp3"
        # except KeyError:
        #     self.edge_arp[IPAddr(arppkt.protosrc)] = EthrAddr(arppkt.hwsrc)
        #     print "in arp4"

        #self.buildHostTable(event, parsedpkt)


