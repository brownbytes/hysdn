#### this is the lastest file created on 6 dec. needs merger

#####
# author : durga
# module : main process flow module 
# date :  6 dec
#####
"""
this is the primary decision making process
the process validates an incoming packet as either a control paceket or a data packet
for all control packets, based on whether they are a routing update or LLDP packet
the respective tables are updated.
for routing updates, the packet is forwarded to the RS to update the routing tables
incase of LLDP packets, lldp packets corresponding actions are taken

for all data packets, based on the destination address , either local address module 
to track addresses is refered to or the routing table is refered for next egress router
also, another table is maintained to map the egree of nodes to the legacy routers to decide a optimal egress node

"""
#####

from pox.lib import *
from pox.core import core
from pox.lib.packet.lldp import * #import all classes from lldp module lldp(),chassis_id(),port_id() etc..
from pox.lib.packet.ethernet import ethernet,ETHER_BROADCAST,NDP_MULTICAST
from pox.lib.packet.ipv4 import ipv4
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
from pox.lib.util import dpid_to_str,str_to_dpid
from pox.lib.addresses import EthAddr,IPAddr,IP_ANY,IP_BROADCAST
import pathfinder
import query_route
from lldpv3 import *
import pickle

log = core.getLogger()

#pickle SWITCHES.txt,HOSTS.txt and EDGES.txt'
_dummydict = {}
with open('SWITCHES.txt','r+') as fd3:
        pickle.dump(_dummydict,fd3)
with open('HOSTS.txt','r+') as fd4:
        pickle.dump(_dummydict,fd4)
with open('EDGES.txt','r+') as fd5):
        pickle.dump(_dummydict,fd5)

def launch():
    """ launch and register custome component to core """
    log.debug("in launch...")
    core.registerNew(MainMapMaker)
				
class MainMapMaker(object):
    """ handles the connections and ofp packets """

    def __init__(self):
        """ adds itself as listener to core.openflow and initialises switch_port dict"""
        core.openflow.addListeners(self)
        self.switch_ports = {} #temp stores switches and connected ports on the switch
        self.edge_switch = {} #temp dict to store edge OF switches
        self.lldpobj = MapMaker()
        
    def _handle_ConnectionUp(self,event):
        """ handler for new connections. maintains switch_port dicts"""
        self.connection = event.connection
        self.ofp = event.ofp
        _ports = []
        log.debug("New Connection established to switch %s",dpid_to_str(event.dpid))
        log.debug("new event details %s", str(event))
        self.insertDefaultFlow(event.connection)

        for port in self.ofp.ports:
            _ports.append((port.port_no,port.hw_addr))
        self.switch_ports[dpid_to_str(self.connection.dpid)]= _ports
        log.debug("%s",str(self.switch_ports))
        self.sendLLDP(event)

    def insertDefaultFlow(self,connection):
        """ method to insert default flow of send to controller , called in connectionup handler"""
        msg = of.ofp_flow_mod()
        msg.actions.append(of.ofp_action_output(port=of.OFPP_CONTROLLER))
        connection.send(msg)

    def updateMap(self):
        """ method to  perform updates on map """
        pass

    def _handle_PacketIn(self,event):
        """ handler for packetIn events """
        self.processPacketIn(event)
		
    def processPacketIn(self,event):
        """ method to process incoming packets """
        parsedpkt = event.parsed
        inport = event.port
        srcdpid = event.dpid
        pktpayload = parsedpkt.payload()
        #log.debug("%i --> %s",event.dpid,parsedpkt)
        if parsedpkt.type == parsedpkt.LLDP_TYPE: #process LLDP packet
            print "processing LLDP Packetin"
            self.lldpobj.packetin_LLDP(event)
            
	elif parsedpkt.dst == EthAddr('01:00:5e:00:00:05') or parsedpkt.dst == EthAddr('01:00:5e:00:00:06'):
            print "processing legacy OSPF routing update packet"
            self.sendrouteupdate(event)
            #update the switch_router dict {sw1:[(p1,r1ip),(p2,r2ip)..],sw2:[...]}
            try:
                ospfpkt = pktpayload#IP pkt within the parsedpkt
                self.edge_switch[dpid_to_str(event.dpid)].append((event.port,ospfpkt.srcip))
            except e,KeyError:
                self.edge_switch[dpid_to_str(event.dpid)]=[(event.port,ospfpkt.srcip)]

        elif parsedpkt.type == parsedpkt.IP_TYPE: #  if parsed eth pkt containe IP payload
            print "processing IP data pkt"
            next_hop_node = next_hop(IPAddr(pktpayload.dstip))
            print next_hop_node
            if next_hop_node == 0 : #if the dstip not in RE
                #check if this the format of input to path_finder,src here is mac address
                path = path_finder(pktpayload.src)#find a path between src mac and dst mac
                bestpath = path[pktpayload.dst] # currently path is to all nodes
                #need to modify the same to return best path between src,dst
                if bestpath == none: # if no path is available in OF domain
                    <flood the packet> # flood the packet within OF domain
            elif next_hop_node != 0: # if next hop of the packet exists
                #check the egress node based on the nexthop IP
                # frame a new message
                #send the packet to the egress node to be forwarded onto the interface connecting to the router
                for sw in edge_switch:
                    for each_port in sw:
                       if edge_switch[sw][each_port]==IPAddr(next_hop_node):
                            outport = each_port
                            self.forward_packet(event,outport)#forward the packet onto the outport
                       else:
                           #flood
                           print "no path found"

    def sendLLDP(self,event):
        """ instruct switches to send LLDP packet Out messages ,iterate on switch_port"""
        for outport in self.switch_ports[dpid_to_str(event.dpid)]: # send packeout of lldps on all ports exce$
            #print outport (2.EthAddr('xx:xx:xx:xx:xx:xx')
            msg = of.ofp_packet_out(in_port = of.OFPP_NONE)
            if outport[0] == 65534:
                pass
            else:
                lldpdu = self.lldpobj.buildEther(event.dpid,outport)# (ddpid,2,EthAddr('xx:xx:xx:xx:xx:xx')) 
                msg.actions.append(of.ofp_action_output(port=outport[0]))
                msg.data = lldpdu.pack()
                #log.debug("OF %s",str(msg))
                self.connection.send(msg)
    
    def sendrouteupdate(self,event):
        """ forward the routing update to routing engine via internal 
            ovs quaggaRS """
        routeupdatepkt = event.parsed
        msg = of.ofp_packet_out(in_port = of.OFPP_NONE)
        msg.actions.append(of.ofp_action_output(port=of.OFPP_ALL)) #need to update it as per egress ports
        msg.data = routeupdatepkt.pack()

        for connection in core.openflow.connections:
            log.debug("%s --> %s ",str(connection),str(connection.dpid)) #debug
            if connection.dpid != event.dpid:#dont send to self
                core.openflow.sendToDPID(connection.dpid,msg)
    
    def forward_pkt(self,event,outport):
        """instructs the OF switch to forward the packet onto an egress node"""
        msg = of.ofp_packet_out(inport = of.OFPP_NONE)
        msg.actions.append(of.ofp_action_output(port=outport)
        msg.data = event.data
        self.connection.send(msg)#need to send to the swicth

    def getSwitchPort(self):
        """ retunrs switch _port when called in methods outside this class """
        return self.switch_port



