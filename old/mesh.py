#!/usr/bin/python

import re,sys

from mininet.cli import CLI
from mininet.log import setLogLevel, info, error
from mininet.net import Mininet
from mininet.link import Intf
from mininet.topo import Topo
from mininet.util import dumpNodeConnections
from mininet.node import RemoteController
from functools import partial

class MeshTopo(Topo):
    """ create a mesh topology"""
    def __init__(self,switches,**opts):
        super(MeshTopo,self).__init__(**opts)
        self.no_of_switches = switches
        self.all_switches = [0]
        self.create_topo()


    def create_topo(self):
        info("*** creating a mesh of %d switches***" %self.no_of_switches)
        info('*** adding switches ***')


#        c0 = RemoteController('c0',ip = '127.0.0.1')
        for switch_id in range(1,self.no_of_switches+1):
            switch = self.addSwitch('sw%s' %switch_id)
            self.all_switches.append(switch)

        info('*** adding intra_switch links***')
        for node1 in range(1,len(self.all_switches)):
            for node2 in self.all_switches[node1+1:]:
                self.addLink(self.all_switches[node1],node2)

        info(' *** adding hosts ***')
        for swid in range(1,len(self.all_switches)):
            host = self.addHost('h%s' %swid)
            self.addLink(self.all_switches[swid],host)

topos = {'mesh' :(lambda: MeshTopo(switches = 4))}
