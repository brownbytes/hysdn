################
# author:durga
# takes the self._temp_map as input and returns a seq of nodes constituting best path
# dijikstra's algorithm
""" dist[s] <-- 0 (distance to the source vertex is zero
    for all v in V-{s}
        do dist[v] <-- 100000 (set all intial distances to be infinite)
    S <-- empty (S, the set of visited nodes is initially empty
    Q <-- V (intially Q is a set of all vertices)
    while Q is not Emtpty:
        do u <-- minidistance(Q,dist) (select the element of Q with min.distance)
        S <-- SU{u} (add u node to the list of visited vertices)
        for all v in neighbours[u]:
            do if dist[v] > dist[u] + w(u,v) (if new shortest path found)
            then d[v] <-- d[u] + w(u,v) (set new value of shortest path)
   return dist ( also return path)"""
#####################
dist = {}
graph = {}  # {s:{v1:l1,v2:l2...}...}
vertices = []  # all vertices
neinodes = {}  # immediate neighbours
source = 1

import pickle


# with open('SWITCHES.txt','r') as fd:
#        _temp_map = pickle.load(fd)

def mindistance(Q,dist):
    mini = 100000
    tempdist = []

    for node in Q:  # dist of all vertices in Q to source
        if node in neinodes[source]:  # if a immediate nei of source
            if graph[source][node] <= dist[node]:
                dist[node] = graph[source][node]  # distance from source

    for i in Q:
        tempdist.append(dist[i])
    mini = min(tempdist)

    for i in Q:
        if dist[i] == mini:
            return i

def dijkstra_SPF(graph,source):

    global dist
    best_path = {}
    best_path[source] = [source]

    S = [source]  # set of visited vertices, initially empty with source being only vertex d$
    dist[source] = 0  # dist[v] = d # v is vertex and d is associated distance
    Q = []  # queue of remaining vertices
    #vertices = []  # set of all vertices , can be _temp_map  keys

    for dummy_ver in vertices:
        if dummy_ver != source:
            Q.append(dummy_ver)  # updating Q with all the vertices
            best_path[dummy_ver] = [dummy_ver]

    while len(Q) != 0:  # while all vertices are not visited
        edge = mindistance(Q, dist)  # return the nearest neighbour
        if edge not in S:S.append(edge)  # discover the undiscovered edge
        for dummy_v in neinodes[edge]:  # for each immediate nei
            if dummy_v not in S:
                if dist[dummy_v] > dist[edge] + graph[edge][dummy_v]:
                    dist[dummy_v] = dist[edge] + graph[edge][dummy_v]
                    best_path[dummy_v] = best_path[edge]+[dummy_v]
        Q.remove(edge)  # update the remaning nodes

    return best_path


def find_path(src, destination):
    global vertices
    global neinodes
    global dist
    global graph
    global source

    source = src
    #print src, destination

    path = {}

    with open('SWITCHES.txt','r') as fd:
        _temp_map = pickle.load(fd)
    #print "Temp map",_temp_map
    vertices = [] # list of all chassis
    neinodes = {}
    graph = {}
    for ver in _temp_map:
        neinodes[ver] = []
        graph[ver] = {}
        vertices.append(ver)
        dist[ver] = 100000
        for nei in _temp_map[ver]:
            neinodes[ver].append(nei[1][0])  #  _temp_map[chassis] = [[port1,(chassus2,port2$
            graph[ver][nei[1][0]]= 1
    path = dijkstra_SPF(graph,source)  # computes best paths to all nodes
    #print "path" + str(path)
    try:
        srctodst = path[destination]
        finalpath = formatpath(srctodst, source, _temp_map)
        return finalpath
    except KeyError:
        return None

def formatpath(path, source,_temp_map):
    """input:path;output:sw <-->pr details,sw formatted as dpids"""
    src = source
    bestpath = {}  # {dst1:(src,srcp),dst2:(src1,pr)...}
    #_ temp_map = {sw1:[[pr1,{sw2,pr2)],[p2,(sw3,pr3)]..],sw2:[[p1,(sw1,pr1)...]
    for each_node in path:
        for each_nei in _temp_map[src]:
            if each_nei[1][0] == each_node:  # find the immediate next node
                bestpath[each_node]=(src,each_nei[0]) # to next hop
                src = each_node  # update src as current node
    return bestpath


#print find_path(1,4)

#print find_path(4,1)

#print find_path(2,4)

#print find_path(4,2)
