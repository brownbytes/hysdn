"""
example topology of Quagga routers
start4.py + topo9.py
                      R2
                    /    \
        h1--R1==SW1=      =SW2==R4--h2
                    \    /
                      R3
R1:eth0:11.11.11.1/24 ; eth1:12.12.12.1/24 ; eth2:13.13.13.1/24 ; lo 1.1.1.1/24;
R2:eth0:12.12.12.2/24 ; eth1:24.24.24.2/24 ; 2.2.2.2/24
R3:eth0:13.13.13.3/24 ; eth1:34.34.34.3/24 ; 3.3.3.3/24
R4:eth0:11.11.11.4/24 ; eth1:24.24.24.24.4/24 ; eth2:34.34.34.4/24; lo 4.4.4.4/24
"""

import inspect
import os
from collections import namedtuple
from mininet.link import Intf
from mininext.topo import Topo
from mininext.services.quagga import QuaggaService
from mininext.node import Node
from mininet.node import Switch
from mininet.net import Mininet


class QuaggaTopo(Topo):
    """creates the above topology"""

    def __init__(self):
        Topo.__init__(self)
        "directory where this file / script is located"
        selfPath = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) #script directory
        #initialise a service helper for Quagga with default options
        quaggaSvc = QuaggaService(autoStop=False)
        #path config
        quaggaBaseConfigPath = selfPath + '/configs/'


        R1 = self.addHost(name='R1',ip='11.11.11.101/24',hostname='R1',privateLogDir=True,privateRunDir=True,inMountNamespace=True,inPIDNamespace=True,inUTSNamespace=True)
        R2 = self.addHost(name='R2',ip='12.12.12.2/24',hostname='R2',privateLogDir=True,privateRunDir=True,inMountNamespace=True,inPIDNamespace=True,inUTSNamespace=True)
        R3 = self.addHost(name='R3',ip='13.13.13.3/24',hostname='R3',privateLogDir=True,privateRunDir=True,inMountNamespace=True,inPIDNamespace=True,inUTSNamespace=True)
        R4 = self.addHost(name='R4',ip='11.11.11.102/24',hostname='R4',privateLogDir=True,privateRunDir=True,inMountNamespace=True,inPIDNamespace=True,inUTSNamespace=True)

        sw1 = self.addSwitch('sw1')
        sw2 = self.addSwitch('sw2')

        # adding hosts
        h1 = self.addHost('h1')
        h2 = self.addHost('h2')


        # configuring primary links
        self.addLink(R1,sw1,0,1)

        self.addLink(R2,sw1,0,4)
        self.addLink(R2,sw2,1,4)

        self.addLink(R3,sw1,0,5)
        self.addLink(R3,sw2,1,5)

        self.addLink(R4,sw2,0,1)

        self.addLink(R1,h1,3,1)
        self.addLink(R4,h2,3,1)


        quaggaSvcConfig = {'quaggaConfigPath': quaggaBaseConfigPath + 'R1'}
        self.addNodeService(node=R1,service=quaggaSvc,nodeConfig=quaggaSvcConfig)
        self.addNodeLoopbackIntf(node='R1',ip='1.1.1.1/24')

        quaggaSvcConfig = {'quaggaConfigPath': quaggaBaseConfigPath + 'R2'}
        self.addNodeService(node=R2,service=quaggaSvc,nodeConfig=quaggaSvcConfig)
        self.addNodeLoopbackIntf(node='R2',ip='2.2.2.2/24')

        quaggaSvcConfig = {'quaggaConfigPath': quaggaBaseConfigPath + 'R3'}
        self.addNodeService(node=R3,service=quaggaSvc,nodeConfig=quaggaSvcConfig)
        self.addNodeLoopbackIntf(node='R3',ip='3.3.3.3/24')

        quaggaSvcConfig = {'quaggaConfigPath': quaggaBaseConfigPath + 'R4'}
        self.addNodeService(node=R4,service=quaggaSvc,nodeConfig=quaggaSvcConfig)
        self.addNodeLoopbackIntf(node='R4',ip='4.4.4.4/24')



