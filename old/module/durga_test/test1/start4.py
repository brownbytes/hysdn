__author__ = 'root'
#!/usr/bin/python

"""
Example network of Quagga routers
(QuaggaTopo + QuaggaService)
start4.py + topo9.py
"""

import sys
import atexit

# patch isShellBuiltin
import mininet.util
import mininext.util
mininet.util.isShellBuiltin = mininext.util.isShellBuiltin
sys.modules['mininet.util'] = mininet.util

from mininet.util import dumpNodeConnections
from mininet.node import RemoteController
from mininet.log import setLogLevel, info

from mininext.cli import CLI
from mininext.net import MiniNExT

from topo import QuaggaTopo

net = None


def startNetwork():
    "instantiates a topo, then starts the network and prints debug information"

    info('** Creating Quagga network topology\n')
    topo = QuaggaTopo()

    info('** Starting the network\n')
    global net
    net = MiniNExT(topo, controller=RemoteController)
    #net.start() #donnot start the network before

    info('** 1.Dumping host connections\n')
    dumpNodeConnections(net.hosts)

#    info('** Testing network connectivity\n')
#    net.ping(net.hosts)
    routers = net.hosts
    switches = net.switches
    #adding redundant links to switches
    #net.addLink(routers[0],switches[0],0,1) #R1(0)==SW1(1)#LRS links
    net.addLink(routers[0],switches[0],1,2)  #R1(1)==SW1(2)
    net.addLink(routers[0],switches[0],2,3)  #R1(2)==SW1(3)

    #net.addLink(routers[3],switches[1],0,1) #R4(0)==SW2(1)#LRS links
    net.addLink(routers[3],switches[1],1,2) #R4(1)==SW2(2)
    net.addLink(routers[3],switches[1],2,3) #R4(2)==SW2(3)

    #these links are already configured in topo8.py

    #net.addLink(routers[1],switches[0],0,4) #R2(0)--sw1(4)
    #net.addLink(routers[1],switches[1],1,4) #R2(1)--sw2(4)

    #net.addLink(routers[2],switches[0],0,5) #R3(0)--sw1(5)
    #net.addLink(routers[2],switches[1],1,5) #R3(1)--sw2(5)

    net.start()

    info('*** 2.dumping host connections\n')
    dumpNodeConnections(net.hosts)

    info('** Dumping host processes\n')
    for host in net.hosts:
        host.cmdPrint("ps aux")

    info('** Running CLI\n')
    CLI(net)

def stopNetwork():
    "stops a network (only called on a forced cleanup)"

    if net is not None:
        info('** Tearing down Quagga network\n')
        net.stop()

if __name__ == '__main__':
    # Force cleanup on exit by registering a cleanup function
    atexit.register(stopNetwork)

    # Tell mininet to print useful information
    setLogLevel('info')
    startNetwork()
