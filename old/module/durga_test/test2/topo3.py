"""
example topology of QUagga routers

R1 <--> SW1 <--> SW3 <--> SW2 <--> R2
	 |      /   \      |
         |     /     \     |
         |    /       \    |
        SW4---         --- SW5
         |                 |
         h1                h2

R1:11.11.11.33/24 ; 3.3.3.3/24
R2:11.11.11.44/24 ; 4.4.4.4/24 
"""

import inspect 
import os
from collections import namedtuple

from mininext.topo import Topo
from mininext.services.quagga import QuaggaService

QuaggaHost = namedtuple("QuaggaHost","name ip loIP")
net= None

class QuaggaTopo(Topo):
    """creates the above topology"""

    def __init__(self):
        Topo.__init__(self)        
        "directory where this file / script is located"
        selfPath = os.path.dirname(os.path.abspath(
            inspect.getfile(inspect.currentframe()))) #script directory
        
        #initialise a service helper for Quagga with default options
        quaggaSvc = QuaggaService(autoStop=False)
        
        #path config
        quaggaBaseConfigPath = selfPath + '/configs/'
   
        #list of quagga host configs
        quaggaHosts = []
        quaggaHosts.append(QuaggaHost(name='R1',ip='11.11.11.33/24',
                                      loIP='3.3.3.3/24'))
        quaggaHosts.append(QuaggaHost(name='R2',ip='11.11.11.44/24',
                                      loIP='4.4.4.4/24'))
      
        #adding OF switches to the topology
        ofsw1 = self.addSwitch('sw1')
        ofsw2 = self.addSwitch('sw2')
        ofsw3 = self.addSwitch('sw3')
        ofsw4 = self.addSwitch('sw4')
        ofsw5 = self.addSwitch('sw5')

        #add links
        self.addLink(ofsw1,ofsw3)
        self.addLink(ofsw3,ofsw2)
        self.addLink(ofsw3,ofsw4)
        self.addLink(ofsw2,ofsw5)
        self.addLink(ofsw1,ofsw4)
        self.addLink(ofsw3,ofsw5)

        ofsws = [ofsw1,ofsw2,ofsw3,ofsw4,ofsw5]
        
        #creating hosts
        ofsw4h1 = self.addHost(name='h1')
        ofsw5h2 = self.addHost(name='h2')

        #adding h1 to sw4 and h2 to sw5
        self.addLink(ofsw4,ofsw4h1)
        self.addLink(ofsw5,ofsw5h2)
        i = 0
        
        for host in quaggaHosts:
        
            quaggaContainer = self.addHost(name=host.name,ip=host.ip,hostname=host.name,privateLogDir=True,privateRunDir=True,inMountNamespace=True,inPIDNamespace=True,inUTSNamespace=True)
            self.addNodeLoopbackIntf(node=host.name,ip=host.loIP)
            quaggaSvcConfig = {'quaggaConfigPath': quaggaBaseConfigPath + host.name}
            self.addNodeService(node=host.name,service=quaggaSvc,nodeConfig=quaggaSvcConfig)
            self.addLink(quaggaContainer,ofsws[i])
            i += 1
