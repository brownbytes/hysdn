###############
#query a route from route server 
##############

import subprocess #to run linux commands

def next_hop_of(addr):
    cmd1 = ['lxc-attach','-n','RS2','-e','--','ip','-s','route','get',addr] #RS2 to be substituted by the name of the lxc container
    cmd2 = ['cut','-f','3','-d',' ']

    p1 = subprocess.Popen(cmd1,stdout=subprocess.PIPE)
    p2 = subprocess.Popen(cmd2,stdin=p1.stdout,stdout=subprocess.PIPE)
    p1.stdout.close()

    #output1,err1 = p1.communicate()  
    output2,err2 = p2.communicate()  

  #  print "O1",output1
#    print "O2",output2

    if 'unreachable' in output2:
        return 0
    else: # if there is a route for the dst ip
        return output2 #return the next hop
 
    

#o = next_hop_of('1.1.1.1')
#print o
