__author__ = 'root'
#####
# author : durga
# module : main process flow module
# date :  20 may
#####
"""
this is the primary decision making process
the process validates an incoming packet as either a control packet or a data packet
for all control packets, based on whether they are a routing update or LLDP packet
the respective tables are updated.
for routing updates, the packet is forwarded to the RS to update the routing tables
incase of LLDP packets, lldp packets corresponding actions are taken

for all data packets, based on the destination address , either local address module
to track addresses is refered to or the routing table is refered for next egress router
also, another table is maintained to map the egress of nodes to the legacy routers to decide a optimal egress node

20 may- adding ofp_port_status handling mechanism

"""

from pox.lib import *
from pox.core import core
from pox.lib.packet.lldp import *  # import all classes from lldp module lldp(),chassis_id(),port_id() etc..
from pox.lib.packet.ethernet import ethernet, ETHER_BROADCAST, NDP_MULTICAST
from pox.lib.packet.ipv4 import ipv4
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
from pox.lib.util import dpid_to_str, str_to_dpid
from pox.lib.addresses import EthAddr, IPAddr, IP_ANY, IP_BROADCAST
import pathfinder
import query_route
from lldpv3 import *
from policymap import matchPolicy
import pox.lib.packet as pkt

import pickle
import sys
import linecache


log = core.getLogger()


def tracer(frame, event, arg):
    """ setting a trace function to check the event flows"""
    filename = frame.f_code.co_filename
    linenumber = frame.f_lineno
    funcname = frame.f_code.co_name
    line = linecache.getline(filename, linenumber)
    info = "%s: %s in %s" % (linenumber, line, funcname)
    print info


ethertypes = ['IP_TYPE', 'ARP_TYPE', 'RARP_TYPE', 'VLAN_TYPE', 'LLDP_TYPE', 'PAE_TYPE', 'MPLS_TYPE', 'MPLS_MC_TYPE',
              'IPV6_TYPE', 'PPP_TYPE', 'LWARPP_TYPE', 'GSMP_TYPE', 'IPX_TYPE', 'IPX_TYPE', 'WOL_TYPE', 'TRILL_TYPE',
              'JUMBO_TYPE', 'SCSI_TYPE', 'ATA_TYPE', 'QINQ_TYPE']


def launch():
    """ launch and register custom component to core """
    log.debug("in launch...")
    core.registerNew(MainMapMaker)


class MainMapMaker(object):
    """handles the connections and ofp packets"""

    def __init__(self):
        """ adds itself as listener to core.openflow and initialises switch_port dict"""
        core.openflow.addListeners(self)
        self.switch_ports = {}  # temp stores switches and connected ports on the switch
        self.edge_switch = {}  # temp dict to store edge OF switches
        self.lldpobj = MapMaker()
        self.switch_hosts = {}  # stores OFswitches and associated hosts IPs
        self.edge_arp = {}  # stores edge IP MAC mapping details

    def _handle_ConnectionUp(self, event):
        """ handler for new connections. maintains switch_port dicts"""

        self.connection = event.connection
        #self.ofp = event.ofp
        #_ports = []
        log.debug("New Connection established to switch %s", dpid_to_str(event.dpid))
        log.debug("new event details %s", str(event))

        self.insertDefaultFlow(event.connection)

        self.updateSwPorts(event) # update switch port details

        #for port in self.ofp.ports:
        #    _ports.append((port.port_no, port.hw_addr))
        #self.switch_ports[event.dpid] = _ports
        #log.debug("%s", str(self.switch_ports))
        self.sendLLDP(event)

    def _handle_ConnectionDown(self, event):
        """ handler if controller losses connectivity with a switch"""
        pass

    def _handle_PortStatus(self,event):
        """ handler for portstatus """
        #print "in port status"
        self.checkPortStatus(event)

    ##### connection establishment and legacy communication

    def insertDefaultFlow(self,connection):
        """ method to insert default flow of send to controller , called in connectionup handler"""
        msg = of.ofp_flow_mod()
        msg.priority = 1
        msg.match = of.ofp_match(dl_type = pkt.ethernet.IP_TYPE, nw_proto = 89) #inserting default flow for OSPF
        msg.actions.append(of.ofp_action_output(port=of.OFPP_CONTROLLER))
        connection.send(msg)


    def updateMap(self):
        """ method to  perform updates on map """
        pass

    def _handle_PacketIn(self, event):
        """ handler for packetIn events """
        # sys.settrace(tracer)
        #print "event.dpid %s" %(event.dpid)
        self.processPacketIn(event)
        # sys.settrace(None)

    def processPacketIn(self, event):
        """ method to process incoming packets """
        parsedpkt = event.parsed
        inport = event.port
        srcdpid = event.dpid
        pktpayload = parsedpkt.payload
        target_sw = {}
        isedge = 0
        #print str(srcdpid)
        #log.debug("%i --> %s", event.dpid, parsedpkt)

        # handling LLDP packet
        if parsedpkt.type == parsedpkt.LLDP_TYPE:
            # print "processing LLDP Packetin"
            self.lldpobj.packetin_LLDP(event)

        # handling OSPF multicast packets

        elif parsedpkt.dst == EthAddr('01:00:5e:00:00:05') or parsedpkt.dst == EthAddr('01:00:5e:00:00:06'):  # 2
            #print "processing OSPF data pkt from %s" %(event.dpid)
            self.buildEdgeTable(event)  # builds edge table
            # update the switch_router dict {sw1:[(p1,r1ip),(p2,r2ip)..],sw2:[...]}
            # multicast the packet
            target_sw = self.multicastPacket(parsedpkt)
            self.controllerQueue(event, target_sw)  # controller queues the packet

        # handling ARP packets
        elif parsedpkt.type == parsedpkt.ARP_TYPE:
            arppkt = parsedpkt.payload
            edge_sw = self.findEgress(arppkt.protodst)
            # print "processing ARP ", edge_sw, arppkt.protodst
            if edge_sw != (0, 0):  # if edge switch, the controller enqueues the packet
                target_sw[edge_sw[0]] = [edge_sw[1]]
                self.controllerQueue(event, target_sw)
                self.buildEdgeARP(arppkt)

            elif edge_sw == (0, 0):  # if not an edge device
                self.buildHostTable(event, parsedpkt)  # build host table
                try:  # if dstip in hosttable
                    sw = self.switch_hosts[arppkt.protodst]  # tht is our target sw
                    target_sw[sw[0]] = [sw[1]]
                    self.controllerQueue(event, target_sw)
                except KeyError:  # if not found in host table multicast it
                    target_sw = self.multicastPacket(parsedpkt)
                    self.controllerQueue(event, target_sw)

        elif parsedpkt.type == parsedpkt.IP_TYPE:
            policyflag = False
            #print "processing IP data pkt"
            ippkt = parsedpkt.payload
            print "ippkt:::" +str(ippkt.srcip)+str(ippkt.dstip)+str(ippkt.protocol)
            datapath = self.verifyPolicy(event.dpid,ippkt.srcip,ippkt.dstip,ippkt.protocol)
            #print "processing %s IP data pkt to %s" %(ippkt.protocol, ippkt.dstip)
            #print "datapath1:::"+str(datapath)

            if datapath:
                print " in datapath"
                if event.dpid == datapath[0]: # if this is the source
                    policyflag = True
                    self.forwardAltEdge(event,datapath[1])
                #self.installFlows(datapath, parsedpkt, event)
                else:
                    print "event.dpid == datapath[0]"

            if not(policyflag):
                print "1..in if not(policyflag)"
                if ippkt.protocol == ippkt.TCP_PROTOCOL or ippkt.protocol == ippkt.UDP_PROTOCOL: # if not network packet, but an application packet
                    tpkt = ippkt.payload
                    if tpkt.srcport == 68:# drop dhcp
                        #pass
                        self.dropPackets()
                    # various other application protocols such as http, smtp, pop and others can be handled here can be handled here

                else: # if a network packet like ICMP, OSPF then handle as below
                    target_sw = self.unicastPacket(parsedpkt)
                    print "3.. "
                    print "***unicast TS***", target_sw
                    if target_sw != {}:  # if unicast finds a target sw
                        print "4.."
                        if ippkt.protocol == 89: # refer page 197 of notes
                            self.controllerQueue(event, target_sw)
                        else:
                            self.controllerFlowInstall(event,target_sw)

                        #egress = self.findEgress(ippkt.dstip)
                        #if egress != (0,0): # if the dst ip is connected to a edge sw
                        #    self.controllerFlowInstall(event,egress[0]) # added due to h1 --> 24.24.24.4# doesnot update RS
                            #self.controllerQueue(event, egress[0])  #can be replaced by flowinstall
                        #elif egress == (0,0): # if not directly connected to edge , then find path and install flows on all switches
                        #    print "5.."
                        #    self.controllerFlowInstall(event, target_sw)
                        #else:
                        #    print "is edge ==0 if "
                    elif target_sw == {}:  # if unicasting did not find target_Sw multicast the packet
                        print "multicasting"
                        target_sw = self.multicastPacket(parsedpkt)
                        print "mst:" + str(target_sw)
                        self.controllerQueue(event, target_sw)

    def sendLLDP(self, event):
        """ instruct switches to send LLDP packet Out messages ,iterate on switch_port"""
        # print "Sending LLDP"
        for outport in self.switch_ports[event.dpid]:  # send packetout of lldps on all ports exce$
            # print outport (2.EthAddr('xx:xx:xx:xx:xx:xx')
            msg = of.ofp_packet_out(in_port=of.OFPP_NONE)
            if outport[0] == 65534:
                pass
            else:
                lldpdu = self.lldpobj.buildEther(event.dpid, outport)  # (ddpid,2,EthAddr('xx:xx:xx:xx:xx:xx'))
                msg.actions.append(of.ofp_action_output(port=outport[0]))
                msg.data = lldpdu.pack()
                log.debug("OF %s", str(msg))
                self.connection.send(msg)



    def findEgress(self, ipaddr):
        """checks if the IP address belongs to any of the border routers and returns the sw and port which connect to it"""
        is_edge = 0  # flag to indicate whether the ip addr is found in the
        # print "finding egress to %s" %(ipaddr)
        for each_sw in self.edge_switch:
            for each_pr in self.edge_switch[each_sw]:
                if each_pr[1] == ipaddr:
                    outport = each_pr[0]
                    egrssw = each_sw
                    is_edge = 1
                    break
                else:
                    continue
        if is_edge == 1:  # the node is edge router
            return egrssw, outport

        elif is_edge == 0:  # implies internal network, check if next hop exists using routeserver
            return (0, 0)

    def buildHostTable(self, event, parsedpkt):
        """ builds host table"""

        # parsedpkt = event.parsed
        # extract arp packet
        arppkt = parsedpkt.payload
        srcip = arppkt.protosrc
        # do source based learning for ARP

        try:
            if self.switch_hosts[srcip] == (event.dpid, event.port):  # srcip exists for this switch
                pass  # do nothing
            else:
                self.switch_hosts[srcip] = (event.dpid, event.port)  # update latest information
        except KeyError:
            self.switch_hosts[srcip] = (event.dpid, event.port)

            # print "building host table",self.switch_hosts

    def installFlows(self, dstswid, parsedpkt, event):
        """install flows  on  the switches """
        #pktpayload = parsedpkt.payload

        msg = of.ofp_flow_mod()
        msg.match = of.ofp_match.from_packet(parsedpkt, event.port)
        msg.idle_timeout = 10
        # msg.match.dl_type = 0x800
        # msg.match.nw_dst = IPAddr(ippkt.dstip)
        swdpid = dstswid[0]  # source switch dpid
        outport = dstswid[1]  # source switch outport
        #print "installing flows on"  +str(swdpid) + "for port" + str(outport)

        # insert flowmod of a specific destination IP, outgoing on this port, on switchdpid

        msg.actions.append(of.ofp_action_output(port=outport))
        msg.data = event.ofp
        # print "swdpid %s,outport%s" %(str(swdpid),str(outport))
        core.openflow.sendToDPID(swdpid, msg)

    def installFlowsRemote(self, dstswid, parsedpkt, event):
        """based on the dstip update the destination mac addr feild of the packet"""
        pktpayload = parsedpkt.payload
        next_hop = query_route.next_hop_of(str(IPAddr(pktpayload.dstip)))
        tmac = self.edge_arp[IPAddr(next_hop)]
        #print tmac, type(tmac)
        swdpid = dstswid[0]  # source switch dpid
        outport = dstswid[1]

        msg = of.ofp_flow_mod()
        msg.match = of.ofp_match.from_packet(parsedpkt, event.port)
        msg.actions.append(of.ofp_action_dl_addr.set_dst(tmac))
        msg.actions.append(of.ofp_action_output(port=outport))
        msg.data = event.ofp
        # print "swdpid %s,outport%s" %(str(swdpid),str(outport))
        core.openflow.sendToDPID(swdpid, msg)


    def getSwitchPort(self):
        """ returns switch _port when called in methods outside this class """
        return self.switch_port

    def multicastPacket(self, parsedpkt):
        """returns the traget_sw on which controller should perform some activity"""
        target_sw = {}
        dontMC = []
        # print "3. multicasting the packet to other switches"
        with open('SWITCHES.txt', 'r+') as fd8:
            temp_switch = pickle.load(fd8)
            pickle.dump(temp_switch, fd8)

        # determing all edge switches and ports connected to routers as target sws for OSPF packets
        if parsedpkt.dst == EthAddr('01:00:5e:00:00:05') or parsedpkt.dst == EthAddr('01:00:5e:00:00:06'):
            for each_sw in self.edge_switch:
                for each_router in self.edge_switch[each_sw]:
                    try:
                        pr = target_sw[each_sw]
                        if each_router[0] in pr:
                            pass
                        else:
                            target_sw[each_sw].append(each_router[0])
                    except KeyError:
                        target_sw[each_sw] = [each_router[0]]

        # if not edge , multicast to all switches except the ports of switches that connect to other switches
        else: #elif parsedpkt.dst == EthAddr('ff:ff:ff:ff:ff:ff'):
            for each_sw in temp_switch:
                for ports in temp_switch[each_sw]:
                    dontMC.append((each_sw, ports[0]))  # dont multicast on these ports

            for each_sw in self.switch_ports:
                for each_pr in self.switch_ports[each_sw]:
                    #print "each_pr", each_pr
                    if (each_sw, each_pr[0]) in dontMC:
                        pass
                    else:
                        try:
                            pr = target_sw[each_sw]
                            if each_pr in pr:
                                pass
                            else:
                                target_sw[each_sw].append(each_pr[0])
                        except KeyError:
                            target_sw[each_sw] = [each_pr[0]]
        print "in multicasting", target_sw
        return target_sw


    def unicastPacket(self, parsedpkt):
        """ returns target sw for unicasting, returns an edge sw or a of swwitch connecting to host or an edge sw for legacy """
        pktpayload = parsedpkt.payload
        target_sw = {}
        print "unicast",self.switch_hosts, type(pktpayload.dstip)
        print self.edge_switch

        # check if edge device to unicast
        for each_sw in self.edge_switch:
            for each_r in self.edge_switch[each_sw]:
                if each_r[1] == pktpayload.dstip:
                    target_sw[each_sw] = [each_r[0]]
                    print "unicast TS1",target_sw
                    return target_sw

        # if not an edge, check host table
        if pktpayload.dstip in self.switch_hosts:
            target_sw[self.switch_hosts[pktpayload.dstip][0]] = [self.switch_hosts[pktpayload.dstip][1]]
            print "unicast TS2", target_sw
            return target_sw

        # if part of remote legacy network , compute edge sw as target sw
        else:
            next_hop_node = query_route.next_hop_of(str(IPAddr(pktpayload.dstip)))
            print next_hop_node
            if next_hop_node != 0:
                edge_sw = self.findEgress(IPAddr(next_hop_node))
                print "edge_sw"+str(edge_sw)
                if edge_sw != (0, 0):
                    target_sw[edge_sw[0]] = [edge_sw[1]]
                    print "unicast TS3", target_sw
                    return target_sw

        return target_sw

    def controllerQueue(self, event, target_sw):
        """controller directly queues the packet to target_sw"""
        print "in controller queue",target_sw,event.parsed
        for sw in target_sw:  # send on all ports except the incoming port
            for pr in target_sw[sw]:
                msg = of.ofp_packet_out(in_port=of.OFPP_CONTROLLER)
                msg.buffer_id = of.NO_BUFFER
                msg.data = event.parsed.pack()
                if (pr, sw) != (event.port, event.dpid):
                    print "controller queing packet now"
                    msg.actions.append(of.ofp_action_output(port=pr))
                    core.openflow.sendToDPID(sw, msg)


    def controllerFlowInstall(self, event, target_sw):
        """ controller installs flows on all the switches leading to target_sw"""
        parsedpkt = event.parsed
        dstipaddr = parsedpkt.payload.dstip
        r=[]
        print target_sw
        for sw1 in target_sw:  # for each target switch
            path = pathfinder.find_path(event.dpid, sw1)
            if path == None:  # in case of disconnected OF network
                # find other L3 device connected to the source edge , this can later be extended to find connected non OF l2 switches as well
                print self.edge_switch
                try:
                    l3devices = self.edge_switch[event.dpid]
                    # print "l3devices" + str(l3devices)
                    print " **1"
                    for l3device in l3devices:
                        if l3device[0] == event.port:  # incoming port is same
                            print " **2"
                            ip1 = l3device[1]  # find one interface of the network between 2 routers
                        else:  # else forward the packet onto the connecting L3 device in same network
                            r.append(l3device)  # find all nei Ip addresses
                    # print r
                    for ip2 in r: # for each ip add in r
                        if self.matchIP(ip1,ip2[1]):  # if the nei belong same network as ip1
                            print "****in match****"
                            self.forward_pkt(event,ip2[0])  # forward the packet onto a port connecting to neighbouring l3 device.
                        else:
                            print "self.matchIP(ip1,ip2[1])"
                except Exception,e:
                    logging.exception(e)

            else:  # if there is a continuous OF path then install
                self.installFlows((sw1, target_sw[sw1][0]), parsedpkt, event)
                for sw2 in path:  # for each intermediate node in the path to target switch
                    self.installFlows((path[sw2][0], path[sw2][1]), parsedpkt, event)
                    if sw2 in self.edge_switch:  # if the switch in the path is an edge switch
                        try :
                            sw = self.switch_hosts[dstipaddr]  # if destination belongs to a host. then don't execute remote flow install configuration
                        except KeyError:
                            try:
                                if not (self.isedge(dstipaddr)):  # incase destination is remote location beyond edge routers
                                    self.installFlowsRemote((path[sw2][0], path[sw2][1]), parsedpkt, event)  # change the mac addr and send the packet
                                else:
                                    print "not (self.isedge(dstipaddr))"
                            except Exception,e:
                                logger.exception(e)
            # else:
            #     self.installFlows((path[sw2][0], path[sw2][1]), parsedpkt, event)

    def isedge(self, ipaddr):
        """determines if an address is edge router interface or not"""
        for each_sw in self.edge_switch:
            for each_r in self.edge_switch[each_sw]:
                if each_r[1] == ipaddr:
                    return True

    def buildEdgeTable(self, event):
        """build edge table {dpid:[(swport,packetip)]}"""
        parsedpkt = event.parsed.payload
        #print "building edge table for" + str((event.port, parsedpkt.srcip))
        #print "2.building edge table ",self.edge_switch
        try:
            if (event.port, parsedpkt.srcip) in self.edge_switch[event.dpid]:
                pass
            else:
                self.edge_switch[event.dpid].append((event.port, parsedpkt.srcip))
        except KeyError:
            self.edge_switch[event.dpid] = [(event.port, parsedpkt.srcip)]

    def buildEdgeARP(self, arppkt):
        """build edge arp tables {ip:macaddr} using src based learning for arp packets"""

        #print "@@@@@"+str(arppkt.protosrc)+str(arppkt.hwsrc)
        self.edge_arp[IPAddr(arppkt.protosrc)] = EthAddr(arppkt.hwsrc)
        # try:
        #     if arppkt.protosrc in self.edge_arp:
        #         print "in arp1"
        #         if self.edge_arp[IPAddr(arppkt.protosrc)] == EthrAddr(arppkt.hwsrc):
        #             print "in arp2"
        #             pass
        #         else:
        #             self.edge_arp[IPAddr(arppkt.protosrc)] = EthrAddr(arppkt.hwsrc)
        #             print "in arp3"
        # except KeyError:
        #     self.edge_arp[IPAddr(arppkt.protosrc)] = EthrAddr(arppkt.hwsrc)
        #     print "in arp4"

        # self.buildHostTable(event, parsedpkt)

    # 2.port status check methods

    def checkPortStatus(self,event):
        """ checks if a port staus change is due to addition of edge or deletion"""
        if event.ofp.desc.state == of.OFPPS_LINK_DOWN:  # port deleted
            self.removeFun(event)

    def addFun(self,event):
        """ adds an edge to relevant file"""
        self.addOFEdge(event)

    def removeFun(self,event):
        """ removes an edge from the file"""
        self.removeOFEdge(event)
        self.removeLEdge(event)

    def fluctuatingPort(self,event):
        """ check if the port is fluctuating"""
        pass

    def addOFEdge(self,event):
        """adds the relevant edge to the topology map switches.txt by sending LLDP packet"""
        self.sendLLDP(event)

    def removeOFEdge(self,event):
        """ removes the relevant edge from the topology map switches.txt """
        #self.updateSwPorts(event)  # update the switch ports dictionary, update appripriately
        # erorr: for port in event.ofp.ports:AttributeError: 'ofp_port_status' object has no attribute 'ports'
        chassis1 = hex(long(event.dpid))[2:-1]
        with open('SWITCHES.txt', 'r+') as fd9:
            temp_switch = pickle.load(fd9)

        try:
            listof = temp_switch[int(chassis1)]  # check if the switch port combo is in temp switch file
            for pr in listof:
                if pr[0] == event.port:
                    chassis2 = pr[1][0]  # second switch
                    pr2 = pr[1][1]
                    listof.remove(pr)  # remove the first set of sw1.pr1 from sw1,pr1 sw2 pr2  connection
                    listof2 = temp_switch[chassis2]
                    for pr in listof2:
                        if pr[0] == pr2:
                            listof2.remove(pr)  # remove the second set of sw2, pr2 from the connection

        except KeyError:
            pass  # just pass

        #print "ps temp switch",temp_switch
        with open('SWITCHES.txt','r+') as fd:
            pickle.dump(temp_switch, fd)

    def addLEdge(self,event):
        """ add a legacy edge to edges.txt, will be duplicate fun of OSPF packet processing module"""
        pass

    def removeLEdge(self,event):
        """ remove a legacy edge from edges.txt"""
        port = event.port # port on which ofp port
        try:
            listof = self.edge_switch[event.dpid]
            for pr in listof:
                if pr[0] == port: # if any switch in self.edge_switch has this port
                    listof.remove(pr) # remove the port and mapping
        except KeyError: #not an edge switch
            pass # do nothing

    def updateSwPorts(self,event):
        """ upadates switch ports of each switch"""
        self.ofp = event.ofp
        _ports = []
        for port in event.ofp.ports:
            _ports.append((port.port_no,port.hw_addr))
        self.switch_ports[event.dpid] = _ports

    def dropPackets(self):
        """ drop all random unneeded packets """
        message = of.ofp_flow_mod()
        message.match = of.ofp_match(dl_type = pkt.ethernet.IP_TYPE, nw_proto = pkt.ipv4.UDP_PROTOCOL, tp_src = 68)
        #print "###dropping###"
        self.connection.send(message)

    ###### policy handling ####


    def forward_pkt(self, event, outport):
        """instructs the OF switch to forward the packet onto an egress node"""
        msg = of.ofp_flow_mod()
        msg.data = event.ofp
        msg.match = of.ofp_match.from_packet(event.parsed, event.port)
        msg.actions.append(of.ofp_action_output(port=outport))
        print "in forwarding_pkt... "
        event.connection.send(msg)  # need to send to the switch

    def forwardAltEdge(self,event,outport):
        """ find the mac address of the policy determined l3device and instructs the OF switch to forward the packet onto an router as decided by policy """
        print "in forwardAltEdge.."
        try:
            l3routers = self.edge_switch[event.dpid]
            for l3device in l3routers:
                if l3device[0] == outport: #incoming port is same
                    ipadd = l3device[1] #
                    tmac = self.edge_arp[ipadd]
                    #print "****"+str(tmac)
                else:
                   print "l3device[0] == outport"

        except Exception,e:
            logging.exception(e)


        msg = of.ofp_flow_mod()
        msg.data = event.ofp
        msg.match = of.ofp_match.from_packet(event.parsed, event.port)
        msg.actions.append(of.ofp_action_dl_addr.set_dst(tmac))
        msg.actions.append(of.ofp_action_output(port=outport))
        event.connection.send(msg)  # need to send to the switch


    def verifyPolicy(self,dp,srcip,dstip,proto):
        #checking for any preexsting policy between the given source and destination
        policy = matchPolicy(dp,srcip,dstip)
        mflag = False #match flag to check if a policy has been applied or not
        if policy != None: # check for all policy between
            if policy['protocol'] == proto: # if a policy is defined for a specific protocol
                if policy['action'] == 'permit':
                    dpid,port = policy['dpid'],policy['port']
                    #print "****"+str(dpid)+str(port)
                    mflag = True
                else:
                    print "each_policy['action'] == 'permit'"
            else:
                print "each_policy['protocol'] == proto"

            if mflag == True:
                listof = self.edge_switch[dpid] #checking if the port is down by verifying the edge file
                for pr in listof:
                    if pr[0] == port:#if port is up
                        return (dpid,port)
                    else:
                        print "pr[0] == port"
                mflag = False # if port doesnot exist in edge file => the port is down , put the flag down
            else:
                print "mflag == True"

        if mflag == False:# if policy is not found,policy did not match or port is down, normal routing has to be followed for forwarding the packet
            return None
        else:
            print "mflag == False"


    def matchIP(self,ip1,ip2):
        """
        to match if two l3 devices connected to a OF switch, belong to same network .right now this module does a mere string comparision,
        but the proper way of doing same is by subnet masks what current the edge table doesnot store
        since most of my ip address are /24 prefixes, using a static subnet mask
        """
        subnetmask =  '255.255.255.0'
        sl = subnetmask.split('.')

        ip1 = str(ip1).split('.')
        ip2 = str(ip2).split('.')
        flag = False
        #print "** matching ip1 ,ip2" + str(ip1)+str(ip2)
        for i in range(3): # first 3 octets for comparision
            #print "***** subnet"+str(int(sl[i])-int(ip1[i]))+str(int(sl[i])-int(ip2[i]))
            if int(sl[i])-int(ip1[i]) == int(sl[i])-int(ip2[i]):
                flag = True
                continue
            else:
                flag = False
                break
        #print flag

        return flag