######
#author : durga
#date : 9 dec
#####
"""formats the bestpath returned by pathfinderinto what POX understands
 bestpath returned from a source to all destinations in format {dst:list of nodes in best path, src2:list of nodes in best path,...}
"""
import pickle
from pox.lib import *
from pox.lib.util import dpid_to_str,str_to_dpid

with open ('SWITCHES.txt','r') as fd7:
    _temp_map = pickle.load(fd7)
#_temp_map= {sw1:[[pr1,(sw2,pr2)],[p2,(sw3,p3)]..],sw2:[[p1,(sw1,pr1)...]

def choosedst(dst):
    """dijikstra computes bestpaths to all nodes, this module chooses best path to a specific destination"""
    pathtodst = _temp_map[dst]
    return pathtodst

def formatpath(path,source):
    """input : path ; output :sw<-->pr details , sw formatted as dpids"""
    src = source
    bestpath = {} # {dst1:(src,srcp),dst2:(src1,pr)}
    print _temp_map 
    for each_node in path:
        for each_nei in _temp_map[src]:
            if each_nei[1][0] == each_node: # find the immediate next node
                bestpath[each_node]=(src,each_nei[0]) #to nexthop , use pr1 on src
                src = each_node #update src to be current node
    return bestpath


#print formatpath([2,3],1)
            
    

