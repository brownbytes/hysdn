__author__ = 'root'

'''

'''
import csv


def readPolicy():
    """reading .csv file into dictionary and builds a hash of hash tables for quick indexing"""

    policy_dict = {}
    final_dict = {}

    with open ('policy.csv') as policyfd:
        reader = csv.DictReader(policyfd)

        for line in reader:
            #print line
            try:
                #print policy_dict
                #policy_dict[line['source']].update({line['destination']:None})
                policy_dict[line['source']].update({line['sno']:None})
            except KeyError:
                #print policy_dict
                #policy_dict[line['source']]={line['destination']:None} #intialising a dictionary
                policy_dict[line['source']]={line['sno']:None} #intialising a dictionary
            #print policy_dict
            #try:
                #policy_dict[line['source']][line['destination']].update({line['sno']:None})
            #    policy_dict[line['source']][line['sno']].update({line['destination']:None})
            #except:
                #policy_dict[line['source']][line['destination']] ={line['sno']:None}
            #    policy_dict[line['source']][line['sno']] ={line['destination']:None}
            #print policy_dict
            try:
                #policy_dict[line['source']][line['destination']][line['sno']].update({'protocol':int(line['protocol']),'dpid':int(line['dpid']),'action':line['action'],'port':int(line['port'])})
                policy_dict[line['source']][line['sno']].update({'destination':line['destination'],'protocol':int(line['protocol']),'dpid':int(line['dpid']),'action':line['action'],'port':int(line['port'])})

            except:
                #policy_dict[line['source']][line['destination']][line['sno']]={'protocol':int(line['protocol']),'dpid':int(line['dpid']),'action':line['action'],'port':int(line['port'])}
                policy_dict[line['source']][line['sno']]={'destination':line['destination'],'protocol':int(line['protocol']),'dpid':int(line['dpid']),'action':line['action'],'port':int(line['port'])}
            #print policy_dict
            #print final_dict
    return policy_dict

def matchPolicy(dp,srcip,dstip):
    """ returns a policy if a match is found, else returns a None"""
    policy_dict = readPolicy()
    #print policy_dict

    policy = None
    try:
        policy_list = policy_dict[str(srcip)].values()
        for policy in policy_list:
            if policy['destination'] == dstip:
                if policy['dpid'] == dp:
                   return policy
    except KeyError:
        policy = None

    return policy

p = matchPolicy('1','10.10.10.100','40.40.40.100')

print p
#print matchPolicy('1.1.1.1','2.2.2.2')

