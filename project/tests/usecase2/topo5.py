"""
example topology of Quagga routers
R1==SW1
R1:eth0:11.11.11.1/24 ; eth1:14.14.14.1/24 ; 1.1.1.1/24

"""

import inspect 
import os
from collections import namedtuple
from mininet.link import Intf
from mininext.topo import Topo
from mininext.services.quagga import QuaggaService
from mininext.node import Node
from mininet.node import Switch
from mininet.net import Mininet


class QuaggaTopo(Topo):
    """creates the above topology"""

    def __init__(self):
        Topo.__init__(self)        
        "directory where this file / script is located"
        selfPath = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) #script directory
        #initialise a service helper for Quagga with default options
        quaggaSvc = QuaggaService(autoStop=False)
        #path config
        quaggaBaseConfigPath = selfPath + '/configs/'


        R1 = self.addHost(name='R1',ip='1.1.1.1/24',hostname='R1',privateLogDir=True,privateRunDir=True,inMountNamespace=True,inPIDNamespace=True,inUTSNamespace=True)
        sw1 = self.addSwitch('sw1')

        #self.addLink(R1,sw1)
        quaggaSvcConfig = {'quaggaConfigPath': quaggaBaseConfigPath + 'R1'}
        self.addNodeService(node='R1',service=quaggaSvc,nodeConfig=quaggaSvcConfig)



