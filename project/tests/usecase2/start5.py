__author__ = 'root'
#!/usr/bin/python

"""
Example network of Quagga routers
(QuaggaTopo + QuaggaService)
start5.py + topo9.py
date:5aug , ecn enabling and testing
"""

import sys
import atexit

# patch isShellBuiltin
import mininet.util
import mininext.util
mininet.util.isShellBuiltin = mininext.util.isShellBuiltin
sys.modules['mininet.util'] = mininet.util

from mininet.util import dumpNodeConnections
from mininet.node import RemoteController
from mininet.log import setLogLevel, info

from mininext.cli import CLI
from mininext.net import MiniNExT

from topo import QuaggaTopo

net = None


def startNetwork():
    "instantiates a topo, then starts the network and prints debug information"

    info('** Creating Quagga network topology\n')
    topo = QuaggaTopo()

    info('** Starting the network\n')
    global net
    net = MiniNExT(topo, controller=RemoteController)
    #net.start() #donnot start the network before

    info('** 1.Dumping host connections\n')
    dumpNodeConnections(net.hosts)

#    info('** Testing network connectivity\n')
#    net.ping(net.hosts)
    routers = net.hosts[:4]
    #[<Host R1: R1-eth0:11.11.11.101,R1-eth3:None pid=7664> , <Host R2: R2-eth0:12.12.12.2,R2-eth1:None pid=7666> , <Host R3: R3-eth0:13.13.13.3,R3-eth1:None pid=7668> , <Host R4: R4-eth0:11.11.11.102,R4-eth3:None pid=7670> , <Host h1: h1-eth1:10.0.0.5 pid=7671> , <Host h2: h2-eth1:10.0.0.6 pid=7672> ]
    hosts = net.hosts[4:]
    switches = net.switches
    #adding redundant links to switches
    #net.addLink(routers[0],switches[0],0,1) #R1(0)==SW1(1)#LRS links
    net.addLink(routers[0],switches[0],1,2)  #R1(1)==SW1(2)
    net.addLink(routers[0],switches[0],2,3)  #R1(2)==SW1(3)

    #net.addLink(routers[3],switches[1],0,1) #R4(0)==SW2(1)#LRS links
    net.addLink(routers[3],switches[1],1,2) #R4(1)==SW2(2)
    net.addLink(routers[3],switches[1],2,3) #R4(2)==SW2(3)

    #these links are already configured in topo8.py

    #net.addLink(routers[1],switches[0],0,4) #R2(0)--sw1(4)
    #net.addLink(routers[1],switches[1],1,4) #R2(1)--sw2(4)

    #net.addLink(routers[2],switches[0],0,5) #R3(0)--sw1(5)
    #net.addLink(routers[2],switches[1],1,5) #R3(1)--sw2(5)

    net.start()

    info('*** 2.dumping host connections\n')
    dumpNodeConnections(net.hosts)

    info('** Dumping host processes\n')
    # for host in net.hosts:
    # configuring router info
    routers[0].cmdPrint("ifconfig R1-eth2 13.13.13.1 netmask 255.255.255.0")
    routers[0].cmdPrint("ifconfig R1-eth1 12.12.12.1 netmask 255.255.255.0")
    routers[0].cmdPrint("ifconfig R1-eth3 60.60.60.1 netmask 255.255.255.0")

    routers[1].cmdPrint("ifconfig R2-eth0 12.12.12.2 netmask 255.255.255.0")
    routers[1].cmdPrint("ifconfig R2-eth1 24.24.24.2 netmask 255.255.255.0")

    routers[2].cmdPrint("ifconfig R3-eth0 13.13.13.3 netmask 255.255.255.0")
    routers[2].cmdPrint("ifconfig R3-eth1 34.34.34.3 netmask 255.255.255.0")


    routers[3].cmdPrint("ifconfig R4-eth1 24.24.24.4 netmask 255.255.255.0")
    routers[3].cmdPrint("ifconfig R4-eth2 34.34.34.4 netmask 255.255.255.0")
    routers[3].cmdPrint("ifconfig R4-eth3 40.40.40.1 netmask 255.255.255.0")


    # switching off the rf-filter

    routers[0].cmdPrint("sysctl -w net.ipv4.conf.R1-eth1.rp_filter=0")
    routers[0].cmdPrint("sysctl -w net.ipv4.conf.R1-eth2.rp_filter=0")
    routers[0].cmdPrint("sysctl -w net.ipv4.conf.R1-eth3.rp_filter=0")
    routers[0].cmdPrint("sysctl -w net.ipv4.conf.all.rp_filter=0")
    routers[0].cmdPrint("sysctl -w net.ipv4.conf.default.rp_filter=0")


    routers[3].cmdPrint("sysctl -w net.ipv4.conf.R4-eth1.rp_filter=0")
    routers[3].cmdPrint("sysctl -w net.ipv4.conf.R4-eth2.rp_filter=0")
    routers[3].cmdPrint("sysctl -w net.ipv4.conf.R4-eth3.rp_filter=0")
    routers[3].cmdPrint("sysctl -w net.ipv4.conf.all.rp_filter=0")
    routers[3].cmdPrint("sysctl -w net.ipv4.conf.default.rp_filter=0")


    routers[1].cmdPrint("sysctl -w net.ipv4.conf.R2-eth0.rp_filter=0")
    routers[1].cmdPrint("sysctl -w net.ipv4.conf.R2-eth1.rp_filter=0")
    routers[1].cmdPrint("sysctl -w net.ipv4.conf.all.rp_filter=0")
    routers[1].cmdPrint("sysctl -w net.ipv4.conf.default.rp_filter=0")


    routers[2].cmdPrint("sysctl -w net.ipv4.conf.R3-eth0.rp_filter=0")
    routers[2].cmdPrint("sysctl -w net.ipv4.conf.R3-eth1.rp_filter=0")
    routers[2].cmdPrint("sysctl -w net.ipv4.conf.all.rp_filter=0")
    routers[2].cmdPrint("sysctl -w net.ipv4.conf.default.rp_filter=0")


    #configuring the hosts
    hosts[0].cmdPrint("ifconfig h1-eth1 60.60.60.100 netmask 255.255.255.0")
    hosts[0].cmdPrint("route add default gw 60.60.60.1")

    hosts[1].cmdPrint("ifconfig h2-eth1 40.40.40.100 netmask 255.255.255.0")
    hosts[1].cmdPrint("route add default gw 40.40.40.1")


    #turning on ecn on the hosts
    hosts[0].cmdPrint("sysctl -w net.ipv4.tcp_ecn=1")
    hosts[1].cmdPrint("sysctl -w net.ipv4.tcp_ecn=1")

    #since we are emulating hosts as routers, lets turn ecn on on router hosts as well
    routers[0].cmdPrint("sysctl -w net.ipv4.tcp_ecn=1")
    routers[1].cmdPrint("sysctl -w net.ipv4.tcp_ecn=1")
    routers[2].cmdPrint("sysctl -w net.ipv4.tcp_ecn=1")
    routers[3].cmdPrint("sysctl -w net.ipv4.tcp_ecn=1")

   # enabling traffic control on the routers. the buffer limit has been set to 50 for weighted fair queueing.
   #  routers[0].cmdPrint("tc qdisc add dev R1-eth3 root fq_codel limit 20 target 0.005ms interval 0.005ms ecn")
   #  routers[0].cmdPrint("tc qdisc add dev R1-eth0 root fq_codel limit 20 target 0.005ms interval 0.005ms ecn")
   #  routers[0].cmdPrint("tc qdisc add dev R1-eth1 root fq_codel limit 20 target 0.005ms interval 0.005ms ecn")
   #  routers[0].cmdPrint("tc qdisc add dev R1-eth2 root fq_codel limit 20 target 0.005ms interval 0.005ms ecn")
   #
   #  routers[1].cmdPrint("tc qdisc add dev R2-eth0 root fq_codel limit 20 target 0.005ms interval 0.005ms ecn")
   #  routers[1].cmdPrint("tc qdisc add dev R2-eth1 root fq_codel limit 20 target 0.005ms interval 0.005ms ecn")
   #
   #  routers[2].cmdPrint("tc qdisc add dev R3-eth0 root fq_codel limit 20 target 0.005ms interval 0.005ms ecn")
   #  routers[2].cmdPrint("tc qdisc add dev R3-eth1 root fq_codel limit 20 target 0.005ms interval 0.005ms ecn")
   #
   #  routers[3].cmdPrint("tc qdisc add dev R4-eth0 root fq_codel limit 20 target 0.005ms interval 0.005ms ecn")
   #  routers[3].cmdPrint("tc qdisc add dev R4-eth1 root fq_codel limit 20 target 0.005ms interval 0.005ms ecn")
   #  routers[3].cmdPrint("tc qdisc add dev R4-eth2 root fq_codel limit 20 target 0.005ms interval 0.005ms ecn")
   #  routers[3].cmdPrint("tc qdisc add dev R4-eth3 root fq_codel limit 20 target 0.005ms interval 0.005ms ecn")


    # # #enabling traffic control on the routers. the buffer limit has been set to 50 for weighted fair queueing.
    routers[0].cmdPrint("tc qdisc add dev R1-eth3 root handle 10: red limit 2000 min 200 max 600 avpkt 10 burst 31 ecn adaptive bandwidth 10Mbit")
    routers[0].cmdPrint("tc qdisc add dev R1-eth0 root handle 10: red limit 2000 min 200 max 600 avpkt 10 burst 31 ecn adaptive bandwidth 10Mbit")
    routers[0].cmdPrint("tc qdisc add dev R1-eth1 root handle 10: red limit 2000 min 200 max 600 avpkt 10 burst 31 ecn adaptive bandwidth 10Mbit")
    routers[0].cmdPrint("tc qdisc add dev R1-eth2 root handle 10: red limit 2000 min 200 max 600 avpkt 10 burst 31 ecn adaptive bandwidth 10Mbit")

    routers[1].cmdPrint("tc qdisc add dev R2-eth0 root handle 10: red limit 2000 min 200 max 600 avpkt 10 burst 31 ecn adaptive bandwidth 10Mbit")
    routers[1].cmdPrint("tc qdisc add dev R2-eth1 root handle 10: red limit 2000 min 200 max 600 avpkt 10 burst 31 ecn adaptive bandwidth 10Mbit")

    routers[2].cmdPrint("tc qdisc add dev R3-eth0 root handle 10: red limit 2000 min 200 max 600 avpkt 10 burst 31 ecn adaptive bandwidth 10Mbit")
    routers[2].cmdPrint("tc qdisc add dev R3-eth1 root handle 10: red limit 2000 min 200 max 600 avpkt 10 burst 31 ecn adaptive bandwidth 10Mbit")

    routers[3].cmdPrint("tc qdisc add dev R4-eth0 root handle 10: red limit 2000 min 200 max 600 avpkt 10 burst 31 ecn adaptive bandwidth 10Mbit")
    routers[3].cmdPrint("tc qdisc add dev R4-eth1 root handle 10: red limit 2000 min 200 max 600 avpkt 10 burst 31 ecn adaptive bandwidth 10Mbit")
    routers[3].cmdPrint("tc qdisc add dev R4-eth2 root handle 10: red limit 2000 min 200 max 600 avpkt 10 burst 31 ecn adaptive bandwidth 10Mbit")
    routers[3].cmdPrint("tc qdisc add dev R4-eth3 root handle 10: red limit 2000 min 200 max 600 avpkt 10 burst 31 ecn adaptive bandwidth 10Mbit")

    #   host.cmdPrint("ps aux")
    info('** Running CLI\n')
    CLI(net)

def stopNetwork():
    "stops a network (only called on a forced cleanup)"

    if net is not None:
        info('** Tearing down Quagga network\n')
        net.stop()

if __name__ == '__main__':
    # Force cleanup on exit by registering a cleanup function
    atexit.register(stopNetwork)

    # Tell mininet to print useful information
    setLogLevel('info')
    startNetwork()
