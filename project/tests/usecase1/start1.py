#!/usr/bin/python

"""
Example network of Quagga routers
(QuaggaTopo + QuaggaService) topo6.py
"""

import sys
import atexit

# patch isShellBuiltin
import mininet.util
import mininext.util
mininet.util.isShellBuiltin = mininext.util.isShellBuiltin
sys.modules['mininet.util'] = mininet.util

from mininet.util import dumpNodeConnections
from mininet.node import RemoteController
from mininet.log import setLogLevel, info

from mininext.cli import CLI
from mininext.net import MiniNExT

from topo import QuaggaTopo

net = None


def startNetwork():
    "instantiates a topo, then starts the network and prints debug information"

    info('** Creating Quagga network topology\n')
    topo = QuaggaTopo()

    info('** Starting the network\n')
    global net
    net = MiniNExT(topo, controller=RemoteController)
    net.start()

    info('** Dumping host connections\n')
    dumpNodeConnections(net.hosts)

    routers = net.hosts[:2]
    #routers - [R1,R2]
    routers[0].cmdPrint("ifconfig R1-eth1 100.100.100.1 netmask 255.255.255.0")
    routers[1].cmdPrint("ifconfig R2-eth1 200.200.200.1 netmask 255.255.255.0")

    routers[0].cmdPrint("sysctl -w net.ipv4.conf.R1-eth1.rp_filter=0")
    routers[0].cmdPrint("sysctl -w net.ipv4.conf.R1-eth0.rp_filter=0")
    routers[0].cmdPrint("sysctl -w net.ipv4.conf.all.rp_filter=0")
    routers[0].cmdPrint("sysctl -w net.ipv4.conf.default.rp_filter=0")


    routers[1].cmdPrint("sysctl -w net.ipv4.conf.R2-eth1.rp_filter=0")
    routers[1].cmdPrint("sysctl -w net.ipv4.conf.R2-eth0.rp_filter=0")
    routers[1].cmdPrint("sysctl -w net.ipv4.conf.all.rp_filter=0")
    routers[1].cmdPrint("sysctl -w net.ipv4.conf.default.rp_filter=0")


    hosts = net.hosts[2:]
    #hosts = [h1,h2,h3,h4]
    hosts[0].cmdPrint("ifconfig h1-eth1 100.100.100.100 netmask 255.255.255.0")
    hosts[0].cmdPrint("route add default gw 100.100.100.1")

    hosts[1].cmdPrint("ifconfig h2-eth1 200.200.200.200 netmask 255.255.255.0")
    hosts[1].cmdPrint("route add default gw 200.200.200.1")

    hosts[2].cmdPrint("ifconfig h3-eth1 11.11.11.101 netmask 255.255.255.0")
    hosts[2].cmdPrint("route add default gw 11.11.11.33")

    hosts[3].cmdPrint("ifconfig h4-eth1 11.11.11.102 netmask 255.255.255.0")
    hosts[3].cmdPrint("route add default gw 11.11.11.44")



    info('** Running CLI\n')
    CLI(net)


def stopNetwork():
    "stops a network (only called on a forced cleanup)"

    if net is not None:
        info('** Tearing down Quagga network\n')
        net.stop()

if __name__ == '__main__':
    # Force cleanup on exit by registering a cleanup function
    atexit.register(stopNetwork)

    # Tell mininet to print useful information
    setLogLevel('info')
    startNetwork()
