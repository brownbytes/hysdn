"""
example topology of Quagga routers

h1<-->R1 <--> SW1 <--> SW3 <--> SW2 <--> R2<-->h2
	     |      /   \      |
         |     /     \     |
         |    /       \    |
        SW4---         --- SW5
         |                 |
         h1                h2

R1:11.11.11.33/24 ; 100.100.100.1/24 ;3.3.3.3/24
R2:11.11.11.44/24 ; 200.200.200.1/24 ;4.4.4.4/24
h1 - 100.100.100.100/24
h2 - 200.200.200.200/24
"""

import inspect
import os
from collections import namedtuple

from mininext.topo import Topo
from mininext.services.quagga import QuaggaService

QuaggaHost = namedtuple("QuaggaHost","name ip loIP")
net= None

class QuaggaTopo(Topo):
    """creates the above topology"""

    def __init__(self):
        Topo.__init__(self)
        "directory where this file / script is located"
        selfPath = os.path.dirname(os.path.abspath(
            inspect.getfile(inspect.currentframe()))) #script directory

        #initialise a service helper for Quagga with default options
        quaggaSvc = QuaggaService(autoStop=False)

        #path config
        quaggaBaseConfigPath = selfPath + '/configs/'

        # adding routers
        R1 = self.addHost(name='R1',ip='11.11.11.33/24',hostname='R1',privateLogDir=True,privateRunDir=True,inMountNamespace=True,inPIDNamespace=True,inUTSNamespace=True)
        R2 = self.addHost(name='R2',ip='11.11.11.44/24',hostname='R2',privateLogDir=True,privateRunDir=True,inMountNamespace=True,inPIDNamespace=True,inUTSNamespace=True)

        #adding OF switches to the topology
        sw1 = self.addSwitch('sw1')
        sw2 = self.addSwitch('sw2')
        sw3 = self.addSwitch('sw3')
        sw4 = self.addSwitch('sw4')
        sw5 = self.addSwitch('sw5')

        #add hosts
        h1 = self.addHost('h1')
        h2 = self.addHost('h2')
        h3 = self.addHost('h3')
        h4 = self.addHost('h4')

        #add links
        self.addLink(R1,sw1,0,1)
        self.addLink(R2,sw2,0,1)

        self.addLink(sw1,sw3,2,2)
        self.addLink(sw3,sw2,1,2)
        self.addLink(sw3,sw4,3,2)
        self.addLink(sw2,sw5,3,2)
        self.addLink(sw1,sw4,3,1)
        self.addLink(sw3,sw5,4,1)

        self.addLink(R1,h1,1,1)
        self.addLink(R2,h2,1,1)

        self.addLink(sw4,h3,3,1)
        self.addLink(sw5,h4,3,1)


        quaggaSvcConfig = {'quaggaConfigPath': quaggaBaseConfigPath + 'R1'}
        self.addNodeService(node=R1,service=quaggaSvc,nodeConfig=quaggaSvcConfig)
        self.addNodeLoopbackIntf(node='R1',ip='3.3.3.3/24')

        quaggaSvcConfig = {'quaggaConfigPath': quaggaBaseConfigPath + 'R2'}
        self.addNodeService(node=R2,service=quaggaSvc,nodeConfig=quaggaSvcConfig)
        self.addNodeLoopbackIntf(node='R2',ip='4.4.4.4/24')

