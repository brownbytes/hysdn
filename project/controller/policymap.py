__author__ = 'root'

'''

'''
import csv

def getlastSno():
    sno = 0
    with open ('policy.csv','r') as policyfd:
        reader = csv.DictReader(policyfd)
        for line in reader:
            sno += 1
    return sno

def readPolicy():
    """reading .csv file into dictionary and builds a hash of hash tables for quick indexing"""

    policy_dict = {}
    with open ('policy.csv','r') as policyfd:
        reader = csv.DictReader(policyfd)
        for line in reader:
            try:
                #policy_dict[line['source']].update({line['destination']:None})
                policy_dict[line['source']].update({line['sno']:None})
            except KeyError:
                #policy_dict[line['source']]={line['destination']:None} #intialising a dictionary
                policy_dict[line['source']]={line['sno']:None} #intialising a dictionary
            #try:
                #policy_dict[line['source']][line['destination']].update({line['sno']:None})
            #    policy_dict[line['source']][line['sno']].update({line['destination']:None})
            #except:
                #policy_dict[line['source']][line['destination']] ={line['sno']:None}
            #    policy_dict[line['source']][line['sno']] ={line['destination']:None}
            try:
                #policy_dict[line['source']][line['destination']][line['sno']].update({'protocol':int(line['protocol']),'dpid':int(line['dpid']),'action':line['action'],'port':int(line['port'])})
                policy_dict[line['source']][line['sno']].update({'destination':line['destination'],'protocol':int(line['protocol']),'dpid':int(line['dpid']),'action':line['action'],'port':int(line['port']),'type':line['type']})

            except:
                #policy_dict[line['source']][line['destination']][line['sno']]={'protocol':int(line['protocol']),'dpid':int(line['dpid']),'action':line['action'],'port':int(line['port'])}
                policy_dict[line['source']][line['sno']]={'destination':line['destination'],'protocol':int(line['protocol']),'dpid':int(line['dpid']),'action':line['action'],'port':int(line['port']),'type':line['type']}
    return policy_dict

def matchPolicy(dp,srcip,dstip):
    """ returns a policy if a match is found, else returns a None"""
    policy_dict = readPolicy()
    policy = None
    try:
        policy_list = policy_dict[str(srcip)].values()
        for policy in policy_list:
            if policy['destination'] == dstip:
                if policy['dpid'] == dp:
                   return policy
    except KeyError:
        policy = None
    print policy
    return policy


def writePolicy(infodict):
    """ updates policy.csv based on the ecn flagged packets
        info: srcip, dstip, protocol, action, dstdpid, dsport, type
    """
    infodict.update({'sno':getlastSno()+1})
    fieldnames = ['sno','source','destination','protocol','action','dpid','port','type']
    with open ('policy.csv','a') as policyfd: # 'w' writes, 'a' append
        writer = csv.DictWriter(policyfd,delimiter=',',fieldnames=fieldnames)
        writer.writerow(infodict)


#p = matchPolicy('1','10.10.10.100','40.40.40.100')
#print lastSno()
#print p
#print matchPolicy('1.1.1.1','2.2.2.2')

#writePolicy({'protocol': 1,'source':'222.222.222.222','destination': '111.111.111.111', 'dpid': 2, 'action': 'permit', 'type': 'static', 'port': 5})
#writePolicy({'protocol': 1,'source':'322.322.322.322','destination': '123.121.111.111', 'dpid': 2, 'action': 'permit', 'type': 'static', 'port': 5})
#writePolicy({'protocol': 1,'source':'8.8.8.8','destination': '111.111.111.111', 'dpid': 2, 'action': 'permit', 'type': 'dynamic', 'port': 5})
#writePolicy({'protocol': 1,'source':'6.6.6.6','destination': '123.121.111.111', 'dpid': 2, 'action': 'permit', 'type': 'dynamic', 'port': 5})